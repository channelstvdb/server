<?php
require 'lib/Slim/Slim.php';
require 'lib/RedBean/rb.php';

session_cache_limiter(false);
session_start();

include 'inc/setup.php';
include 'inc/functions.php';

ini_set('user_agent', 'ChannelsTVDB ('.BASE_URL.')');

$rep = new rep;
if (DEBUG) {
    R::debug( TRUE );
}

#############
#  - API -  #
#############
$app->map('/api/get_channel', function () use ($app) {
    global $rep, $args;
    $args = new args();
    $country = $args->get_value('country');
    $select = [];
    $left_join = [];
    $left_join_params = [];
    $where = [];
    $where_params = [];
    $props = explode('|',$args->get_value('props'));
    if (in_array('last_update',$props))
    {
        $select[] = ', DATE_FORMAT(FROM_UNIXTIME(c.date), \'%Y-%m-%dT%TZ\') AS last_update';
    }
    if ($grabber_id = $args->get_value('grabber_id'))
    {
        $select[] = ', x.xmltvid';
        $left_join[] = 'LEFT JOIN xmltvids x ON c.channel_id = x.channel_id AND x.current = 1 AND x.grabber_id = ?';
        $left_join_params[] = $grabber_id;
    }
    if ($tvpackage_id = $args->get_value('tvpackage_id'))
    {
        $select[] = ', tvpc.channel_num';
        $left_join[] = 'LEFT JOIN tvpackageschannels tvpc ON tvpc.channel_id = c.channel_id AND tvpc.current = 1 AND tvpc.tvpackage_id = ?';
        $left_join_params[] = $tvpackage_id;
    }
    if ($chan_id = $args->get_value('channel_id'))
    {
        $rep->set_data(array('channel'=>R::getRow('
            SELECT c.channel_id, c.name, cc.country, l.url AS logo'.join("", $select).'
            FROM channels c
            LEFT JOIN logos l ON c.channel_id = l.channel_id AND l.current = 1
            LEFT JOIN channelscountrys cc ON c.channel_id = cc.channel_id AND cc.accepted > 0
            '.join(" ", $left_join).'
            WHERE c.channel_id = ? AND c.accepted > 0 '.join(' ',$where).'
            ORDER BY ( cc.country = ?) DESC', //ORDER BY pour que le resultat soit priortairement celui dans le pays est spécifié
            array_merge($left_join_params, [ $chan_id ], $where_params, [ $country ])
          )));
    }
    elseif ($xmltvid = $args->get_value('xmltvid'))
    {
        $rep->set_data(array('channel'=>R::getRow('
            SELECT c.channel_id, c.name, cc.country, l.url AS logo'.join("", $select).'
            FROM xmltvids xb
            LEFT JOIN channels c ON xb.channel_id = c.channel_id AND c.current = 1
            LEFT JOIN logos l ON xb.channel_id = l.channel_id AND l.current = 1
            LEFT JOIN channelscountrys cc ON xb.channel_id = cc.channel_id AND cc.accepted > 0
            '.join(" ", $left_join).'
            WHERE xb.xmltvid = ? AND xb.accepted > 0 '.join(' ',$where).'
            ORDER BY ( cc.country = ?) DESC', //ORDER BY pour que le resultat soit priortairement celui dans le pays est spécifié
            array_merge($left_join_params, [ $xmltvid ], $where_params, [ $country ] )
          )));
    }
    elseif ($alias = $args->get_value('alias'))
    {
        $alias_cleaner = new alias_cleaner();
        $alias = $alias_cleaner->clean($alias);
        $rep->set_data(array('channel'=>R::getRow('
            SELECT c.channel_id, c.name, cc.country, l.url AS logo'.join("", $select).'
            FROM alias a
            LEFT JOIN channels c ON a.channel_id = c.channel_id AND c.current = 1
            LEFT JOIN logos l ON a.channel_id = l.channel_id AND l.current = 1
            LEFT JOIN channelscountrys cc ON c.channel_id = cc.channel_id AND cc.accepted > 0
            '.join(" ", $left_join).'
            WHERE a.alias = ? AND a.accepted > 0 '.join(' ',$where).'
            ORDER BY ( cc.country = ?) DESC', //ORDER BY pour que le resultat soit priortairement celui dans le pays est spécifié
            array_merge($left_join_params, [ $alias ], $where_params, [ $country ] )
          )));
        if (empty($rep->data['channel']))
        {
            R::exec('
                INSERT INTO aliasunknow (alias, ip, date)
                SELECT ?, ?, ? FROM DUAL
                WHERE NOT EXISTS (
                        SELECT 1
                        FROM aliasunknow
                        WHERE alias = ? AND ip = ?
                    );
                UPDATE aliasunknow SET date = ? WHERE alias = ? AND ip = ?;',
                [ $alias, $_SERVER["REMOTE_ADDR"], time(),
                  $alias, $_SERVER["REMOTE_ADDR"], time(), $alias, $_SERVER["REMOTE_ADDR"] ]
            );
        }
    }
    else
    {
        $rep->error('argument channel_id or alias require');
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/get_channels', function () use ($app) {
    global $rep, $args;
    $args = new args();
    $select = [];
    $left_join = [];
    $left_join_params = [];
    $where = [];
    $where_params = [];
    $props = explode('|',$args->get_value('props'));
    if (in_array('last_update',$props))
    {
        $select[] = ', DATE_FORMAT(FROM_UNIXTIME(c.date), \'%Y-%m-%dT%TZ\') AS last_update';
    }
    /*TODO
    if ($channels_ids = $args->get_value('channels_ids'))
    */
    if ($grabber_id = $args->get_value('grabber_id'))
    {
        $select[] = ', x.xmltvid';
        $left_join[] = 'LEFT JOIN xmltvids x ON c.channel_id = x.channel_id AND x.current = 1 AND x.grabber_id = ?';
        $left_join_params[] = $grabber_id;
    }
    if ($tvpackage_id = $args->get_value('tvpackage_id'))
    {
        $select[] = ', tvpc.channel_num';
        $left_join[] = 'LEFT JOIN tvpackageschannels tvpc ON tvpc.channel_id = c.channel_id AND tvpc.current = 1 AND tvpc.tvpackage_id = ?';
        $left_join_params[] = $tvpackage_id;
        if (!isset($channels_ids))
        {
            /*Les channels ids ne sont pas indiquer on récupère toute les chaines pour le tvpackage_id indiqué*/
            $where[] = 'AND tvpc.tvpackage_id = ?';
            $where_params[] = $tvpackage_id;
        }
    }
    if (isset($tvpackage_id) || isset($channels_ids))
    {
        $rep->set_data(array('channels'=>R::getAll('
            SELECT c.channel_id, c.name, cc.country, l.url AS logo'.join("", $select).'
            FROM channels c
            LEFT JOIN logos l ON c.channel_id = l.channel_id AND l.current = 1
            LEFT JOIN channelscountrys cc ON c.channel_id = cc.channel_id AND cc.accepted > 0
            '.join(" ", $left_join).'
            WHERE 1 '.join(' ',$where),
            array_merge($left_join_params, $where_params )
          )));
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/get_alias', function () use ($app) {
    global $rep, $args;
    $args = new args();
    if ($chan_id = $args->get_value('channel_id'))
    {
        $rep->set_data([
            'channel_id' => $chan_id,
            'alias' => R::getCol('
                SELECT alias
                FROM alias
                WHERE channel_id = ? AND accepted > 0',
                [ $chan_id ]
            )
        ]);
    }
    else
    {
        $rep->error('channel_id require');
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/get_countrys', function () use ($app) {
    global $rep, $args;
    $args = new args();
    if ($chan_id = $args->get_value('channel_id'))
    {
        $rep->set_data([
            'channel_id' => $chan_id,
            'countrys' => R::getCol('
                SELECT country
                FROM channelscountrys
                WHERE channel_id = ? AND accepted > 0',
                [ $chan_id ]
            )
        ]);
    }
    else
    {
        $rep->error('channel_id require');
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/get_grabbers', function () use ($app) {
    global $rep, $args;
    $args = new args();
    $countrys = array();
    if ($c = $args->get_value('country')) $countrys = array_merge($countrys,explode(',',$c));
    if ($c = $args->get_value('countrys')) $countrys = array_merge($countrys,explode(',',$c));
    if (empty($countrys))
    {
        $rep->set_data(array('grabbers'=>R::getAll('
                SELECT grabber_id, name, country, web
                FROM grabbers
                WHERE current = 1'
            )));
    }
    else
    {
        $rep->set_data(array('grabbers'=>R::getAll('
                SELECT grabber_id, name, country, web
                FROM grabbers
                WHERE current = 1 AND country IN ( ? )',
                [ join(', ',$countrys) ]
            )));
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/get_tvpackages', function () use ($app) {
    global $rep, $args;
    $args = new args();
    $data = [];
    $where_params = [];
    $left_join = [];
    $left_join_params = [];
    $i = 0;
    if ($path = $args->get_json_values('path'))
    {
        foreach ($path as $p)
        {
            $i ++;
            $left_join[] = 'LEFT JOIN tvpackagespaths AS t'.$i.' ON t'.$i.'.parent = t'.($i-1).'.id AND t'.($i-1).'.name = ?';
            $left_join_params[] = $p;
        }
    }
    else
    {
        $parent = 0;
    }
    $childrens_path = R::getAll('
            SELECT t'.($i-1).'.id AS parent, t'.$i.'.name
            FROM tvpackagespaths AS t0
            '.join('',$left_join).'
            WHERE t0.parent = 0',
            $left_join_params
        );
    $parent = $childrens_path[0]['parent'];
    $rep->set_data(array(
            'tvpackages' => R::getAll('SELECT tvpackage_id, name, path FROM tvpackages WHERE parent = ? AND current = 1', [ $parent ]),
            'pwd_path' => $path,
            'childrens_path' => array_map(function($a) {return $a['name'];}, $childrens_path)
        ));
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/search_channel', function () use ($app) {
    global $rep, $args;
    $args = new args();
    $q = $args->get_value('q');
    if (count($q) >= 1)
    {
        $rep->set_data(array('channels'=>R::getAll('
            SELECT c.channel_id, c.name, cc.country, l.url AS logo
            FROM alias a
            LEFT JOIN channels c ON a.channel_id = c.channel_id AND c.current = 1
            LEFT JOIN logos l ON a.channel_id = l.channel_id AND l.current = 1
            LEFT JOIN channelscountrys cc ON c.channel_id = cc.channel_id AND cc.accepted > 0
            WHERE a.alias LIKE ? AND a.accepted != 0
            GROUP BY c.name
            LIMIT 20',
            [ "%$q%" ]
          )));
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/search_tvpackages', function () use ($app) {
    global $rep, $args;
    $args = new args();
    if ($q = $args->get_value('q'))
    {
        $rep->set_data([
            'tvpackages' => R::getAll('
                    SELECT tvpackage_id, name, path
                    FROM tvpackages
                    WHERE (name LIKE ? OR path LIKE ?) AND current = 1',
                    [ "%$q%", "%$q%" ])
        ]);
    }
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/set_channels', function () use ($app) {
    //test: http://localhost/Web/api/mythChannels/index.php/api/set_channels?channels=[{"country": "fr", "channel_id": "tF1.fr", "name": "TF1"}, {"country": "fr", "id": "f2.fr", "name": "France 2", "logo": "logo.png", "alias": ["France2", "f2"]}]&email=toto@tit.tutu&format=html
    global $rep, $args;
    $args = new args();
    $contribution = new contribution('New channel proposed');
    $rep->debug('New channel proposed');
    foreach ($args->get_json_values('channels') as $row)
    {
        if (isset($row->channel_id)) $row->channel_id = strtolower($row->channel_id);

        if ((isset($row->channel_id)) && (is_valid_id($row->channel_id)))
        {
            $rep->debug($row->channel_id);

            //CHANNEL_ID and NAME
            $current_channel = R::getRow('
                    SELECT id, channel_id, name
                    FROM channels
                    WHERE channel_id = ? AND current = 1',
                    [ $row->channel_id ]
                );
            if ( //on vérifie que ce n'est pas l'entrée actuelle
                    $current_channel //ancienne entrée présente, spécification de nom obligatoire
                    && (!isset($row->name) || (!$row->name) || ($row->name == $current_channel['name'])) //le nom n'existe pas, ou coresspond
                )
            {
                $rep->warning($row->channel_id.': no change');
            }
            elseif (R::count( //on verifie qu'il n'a pas été déjà proposé (double post)
                    'channels',
                    'accepted = 0 AND channel_id = ? AND name = ?',
                    [
                        $row->channel_id,
                        (isset($row->name) && $row->name) ? $row->name : ( $current_channel ? $current_channel['name']    : '')
                    ]
                ))
            {
                $rep->warning($row->channel_id.': already suggest');
            }
            else
            {
                $channel = R::dispense('channels');
                $channel->channel_id = $row->channel_id;
                $channel->name = (isset($row->name) && $row->name) ? $row->name : ( $current_channel ? $current_channel['name'] : $row->channel_id );
                $channel->date = time();
                $channel->contribution_id = $contribution->id;
                R::store($channel);
            }

            //LOGOS
            if (isset($row->logo) && $row->logo) //le logo est indiqué
            {
                if (R::count(   //on vérifie que le logo n'est pas l'actuel ou déjà proposé
                        'logos',
                        'channel_id = ? AND url = ? AND ( current = 1 OR accepted = 0 ) LIMIT 1',
                        [ $row->channel_id, $row->logo ]
                    ))
                {
                    $rep->warning($row->channel_id.': logo "'.$row->logo.'" no changed or in a pending contribution');
                }
                else
                {
                    $logo = R::dispense('logos');
                    $logo->channel_id = $row->channel_id;
                    $logo->url = $row->logo;
                    $logo->date = time();
                    $logo->contribution_id = $contribution->id;
                    R::store($logo);
                }
            }

            //COUNTRY
            if (!isset($row->countrys) && isset($row->country)) $row->countrys = $row->country;
            if (isset($row->countrys) && ($row->countrys)) //pays indiqué
            {
                if (!is_array($row->countrys)) $row->countrys = array($row->countrys); //pays ne sont pas un tableau
                foreach (array_unique(array_map('strtolower',$row->countrys)) as $c) //on met tout en minuscule et on supprime les doublons avant d'iterer
                {
                    if ($c) { //N'est pas vide
                        if (R::count(   //on vérifie que le pays n'a pas déjà été accepté ou proposé
                                'channelscountrys',
                                'channel_id = ? AND country = ?',
                                [ $row->channel_id, $c ]
                            ))
                        {
                            $rep->warning($row->channel_id.': country "'.$c.'" already accepted or in a pending contribution');
                        }
                        else
                        {
                            $country = R::dispense('channelscountrys');
                            $country->channel_id = $row->channel_id;
                            $country->country = $c;
                            $country->date = time();
                            $country->contribution_id = $contribution->id;
                            R::store($country);
                        }
                    }
                }
            }

            //ALIAS
            if (!isset($row->alias)) $row->alias = [];
            elseif (!is_array($row->alias)) $row->alias = array($row->alias); //les alias ne sont pas un tableau
            if (isset($row->name) && $row->name) $row->alias[] = $row->name; //on ajoute le nom de la chaine
            $row->alias[] = $row->channel_id; //on ajoute l'id de la chaine
            foreach (array_unique(array_map('strtolower',$row->alias)) as $al) //on met tout en minuscule et on supprime les doublons avant d'iterer
            {
                if ($al) //n'est pas vide
                {
                    if (R::count(   //on vérifie que l'alias n'est pas déjà dans la base
                            'alias', 'channel_id = ? AND alias = ?',
                            [ $row->channel_id, $al ]
                      ))
                    {
                        $rep->warning($row->channel_id.': alias already accepted or in a pending contribution');
                    }
                    else
                    {
                        //on cache les valeur dans alias inconnu (inutile d'avoir 40 contributions pour un même alias inconnu
                         R::exec('
                                UPDATE aliasunknow
                                SET hidden = 1
                                WHERE alias = ?',
                                [ $al ]
                            );
                        //on ajoute l'alias
                        $alias = R::dispense('alias');
                        $alias->channel_id = $row->channel_id;
                        $alias->alias = $al;
                        $alias->date = time();
                        $alias->contribution_id = $contribution->id;
                        R::store($alias);
                    }
                }
            }

            //xmltvids
            //test http://localhost/Web/api/mythChannels/index.php/api/set_channels?channels=[{"channel_id":"france4.fr","xmltvids":[{"grabber_id":"kazer.org","xmltvid":"FRA4.KAZER.ORG"}]}]
            if (isset($row->xmltvids))
            {
                if (!is_array($row->xmltvids))
                {
                    $rep->error('xmltvids must be a list [{"grabber_id":"...","xmltvid":"..."},...]');
                }
                else
                {
                    foreach ($row->xmltvids as $x)
                    {
                        if (!isset($x->xmltvid) | !isset($x->grabber_id))
                        {
                            $rep->error('xmltvids must be a list [{"grabber_id":"...","xmltvid":"..."},...]');
                        }
                        else
                        {
                            $x->grabber_id = strtolower($x->grabber_id) ;
                            if (R::count(   //on vérifie que l'xmltvid n'est l'actuel ou qu'il n'a pas déja été proposé
                                    'xmltvids', 'channel_id = ? AND xmltvid = ? AND grabber_id = ? AND (current = 1 OR accepted = 0)',
                                    [ $row->channel_id , $x->xmltvid, $x->grabber_id ]
                                ))
                            {
                                $rep->warning('grabber: '.$x->grabber_id.'; channel: '.$row->channel_id.'; xmltvid '.$x->xmltvid.': no change or already suggest.');
                            }
                            elseif (!R::count('grabbers','grabber_id = ?',[ $x->grabber_id ]))
                            {
                                $rep->error($x->grabber_id.' is an unknow grabber_id');
                            }
                            else
                            {
                                $xmltvid = R::dispense('xmltvids');
                                $xmltvid->channel_id = $row->channel_id;
                                $xmltvid->xmltvid = $x->xmltvid;
                                $xmltvid->grabber_id = $x->grabber_id;
                                $xmltvid->date = time();
                                $xmltvid->contribution_id = $contribution->id;
                                R::store($xmltvid);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $rep->error('id require and must be contain only [a-z0-9._-]. '.json_encode($row).' ignored');
        }
    }
    //Reponse
    $rep->set_data(array(
            'channels' => R::getAll('
                    SELECT channel_id, name
                    FROM channels
                    WHERE contribution_id = ?',
                    [ $contribution->id ]
                ),
            'countrys' => R::getAll('
                    SELECT channel_id, country
                    FROM channelscountrys
                    WHERE contribution_id = ?',
                    [ $contribution->id ]
                ),
            'logos' => R::getAll('
                    SELECT channel_id, url AS logo
                    FROM logos
                    WHERE contribution_id = ?',
                    [ $contribution->id ]
                ),
            'alias' => R::getAll('
                    SELECT channel_id, alias
                    FROM alias
                    WHERE contribution_id = ?',
                    [$contribution->id]
                ),
            'xmltvids' => R::getAll('
                    SELECT channel_id, grabber_id, xmltvid
                    FROM xmltvids
                    WHERE contribution_id = ?',
                    [ $contribution->id ]
                )
        ));
    $rep->contribution_id((
            (
                   (!empty($rep->data['channels']))
                || (!empty($rep->data['countrys']))
                || (!empty($rep->data['logos']))
                || (!empty($rep->data['alias']))
                || (!empty($rep->data['xmltvids']))
            )
            ? $contribution->id
            : -1
        ));
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/set_grabbers', function () use ($app) {
    //Ex: http://localhost/Web/api/mythChannels/index.php/api/set_grabbers?grabbers=[{"grabber_id":"kazer.org","name":"KaZeR","country":"fr","web":"http://www.kazer.org"}]
    global $rep, $args;
    $args = new args();
    $contribution = new contribution('New grabbers proposed');
    $grabbers = $args->get_json_values('grabbers');
    foreach ($grabbers as $row) {
        if (!isset($row->grabber_id))
        {
            $rep->error('grabber_id require ('.json_encode($grabbers).')');
        }
        elseif (!is_valid_id(strtolower($row->grabber_id)))
        {
            $rep->error($row->grabber_id.' is an unvalid id');
        }
        else
        {
            $row->id = strtolower($row->grabber_id);
            if (isset($row->web)) {
                $row->web = strtolower($row->web);
                if ((strpos($row->web,'http://' !== 0)) | (strpos($row->web,'https://' !== 0))) $row->web = 'http://'.$row->web;
            }
            $current_grabber = R::getRow('
                    SELECT name, country, web
                    FROM grabbers
                    WHERE grabber_id = ? AND current = 1',
                    [$row->grabber_id]
                );
            if (//on vérifie que le grabber n'est l'actuel
                $current_grabber
                && ((!isset($row->name))    || ($row->name    == $current_grabber['name']))
                && ((!isset($row->country)) || ($row->country == $current_grabber['country']))
                && ((!isset($row->web))     || ($row->web     == $current_grabber['web']))
            )
            {
                $rep->warning($row->grabber_id.': no change.');
            }
            elseif (R::count( //on verifie qu'il n'a pas été déjà proposé (double post)
                    'grabbers',
                    'grabber_id = ? AND accepted = 0 AND name = ? AND country = ? AND web = ?',
                    [
                        $row->grabber_id,
                        isset($row->name)    ? $row->name    : ( $current_grabber ? $current_grabber['name']    : $row->grabber_id ),
                        isset($row->country) ? $row->country : ( $current_grabber ? $current_grabber['country'] : '' ),
                        isset($row->web)     ? $row->web     : ( $current_grabber ? $current_grabber['web']     : '' )
                    ]
                ))
            {
                $rep->warning($row->grabber_id.': already suggest.');
            }
            else
            {
                $grabber = R::dispense('grabbers');
                $grabber->grabber_id = $row->grabber_id;
                $grabber->name =    isset($row->name)    ? $row->name    : ( $current_grabber ? $current_grabber['name']    : $row->grabber_id );
                $grabber->country = isset($row->country) ? $row->country : ( $current_grabber ? $current_grabber['country'] : '' );
                $grabber->web =     isset($row->web)     ? $row->web     : ( $current_grabber ? $current_grabber['web']     : '' );
                $grabber->date = time();
                $grabber->contribution_id = $contribution->id;
                R::store($grabber);
            }
        }
    }
    //Reponse
    $rep->set_data(array('grabbers' => R::getAll('
            SELECT grabber_id, name, country, web
            FROM grabbers
            WHERE contribution_id = ?',
            [$contribution->id]
        )));
    $rep->contribution_id((!empty($rep->data['grabbers']) ? $contribution->id : -1));
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/set_tvpackages', function () use ($app) {
    //Ex: http://localhost/Web/api/mythChannels/index.php/api/set_tvpackages?tvpackages=[{"tvpackage_id":"gratuit.freebox","name":"Chaine gratuite freebox","path":["France", "IPTV", "Free.fr"],"channels":[{"channel_id":"france2.fr", "channel_num":2}, {"channel_id":"france.fr", "channel_num":3}]}]
    global $rep, $args;
    $args = new args();
    $contribution = new contribution('New tvpackage proposed');
    $tvpackages = $args->get_json_values('tvpackages');
    foreach ($tvpackages as $row)
    {
        if (!isset($row->tvpackage_id))
        {
            $rep->error('tvpackages_id require ('.json_encode($row).')');
        }
        elseif (!is_valid_id(strtolower($row->tvpackage_id)))
        {
            $rep->error($row->tvpackages_id.' is an unvalid id');
        }
        else
        {
            $row->tvpackage_id = strtolower($row->tvpackage_id);
            $row->path = json_encode($row->path);
            $current_tvpackage = R::getRow('
                    SELECT name, path
                    FROM tvpackages
                    WHERE tvpackage_id = ? AND current = 1',
                    [$row->tvpackage_id]
                );
            if (//on vérifie que ce n'est l'actuel
                $current_tvpackage
                && ((!isset($row->name)) || ($row->name == $current_tvpackage['name']))
                && ((!isset($row->path)) || ($row->path == $current_tvpackage['path']))
            )
            {
                $rep->warning($row->tvpackage_id.': no change.');
            }
            elseif (R::count( //on verifie qu'il n'a pas été déjà proposé (double post)
                    'tvpackages',
                    'tvpackage_id = ? AND accepted = 0 AND name = ? AND path = ?',
                    [
                        $row->tvpackage_id,
                        isset($row->name) ? $row->name : ( $current_tvpackage ? $current_tvpackage['name'] : $row->tvpackage_id ),
                        isset($row->path) ? $row->path : ( $current_tvpackage ? $current_tvpackage['path'] : '' )
                    ]
                ))
            {
                $rep->warning($row->tvpackage_id.': already suggest.');
            }
            else
            {
                $tvpackage = R::dispense('tvpackages');
                $tvpackage->tvpackage_id = $row->tvpackage_id;
                $tvpackage->name =    isset($row->name) ? $row->name : ( $current_tvpackage ? $current_tvpackage['name']    : $row->tvpackage_id );
                $tvpackage->path = isset($row->path) ? $row->path : ( $current_tvpackage ? $current_tvpackage['path'] : '' );
                $tvpackage->date = time();
                $tvpackage->contribution_id = $contribution->id;
                R::store($tvpackage);
            }

        }
        if (isset($row->channels))
        {
            if (!is_array($row->channels))
            {
                $rep->error('channels must be a list');
            }
            else
            {
                foreach ($row->channels as $c)
                {
                    $c->channel_id = strtolower($c->channel_id) ;
                    $current_tvpackagechannel = R::getRow('
                            SELECT channel_num
                            FROM tvpackageschannels
                            WHERE tvpackage_id = ? AND channel_id = ? AND current = 1',
                            [$row->tvpackage_id, $c->channel_id]
                        );
                    if (//on vérifie que ce n'est l'actuel
                            $current_tvpackagechannel
                        && ((!isset($c->channel_num)) || ($c->channel_num == $current_tvpackagechannel['channel_num']))
                    )
                    {
                        $rep->warning($c->channel_id.' for '.$row->tvpackage_id.': no change.');
                    }
                    elseif (R::count( //on verifie qu'il n'a pas été déjà proposé (double post)
                            'tvpackageschannels',
                            'tvpackage_id = ? AND channel_id = ? AND accepted = 0 AND channel_num = ?',
                            [
                                $row->tvpackage_id,
                                $c->channel_id,
                                isset($c->channel_num) ? $c->channel_num : ( $current_tvpackagechannel ? $current_tvpackagechannel['channel_num'] : '' )
                            ]
                        ))
                    {
                        $rep->warning($c->channel_id.' for '.$row->tvpackage_id.': already suggest.');
                    }
                    elseif (!R::count('channels','channel_id = ? AND current = 1',[ $c->channel_id ]))
                    {
                        $rep->error($c->channel_id.' in '.$row->tvpackage_id.' is unknow channel_id');
                    }
                    else
                    {
                        $tvpackagechannel = R::dispense('tvpackageschannels');
                        $tvpackagechannel->channel_id = $c->channel_id;
                        $tvpackagechannel->tvpackage_id = $row->tvpackage_id;
                        $tvpackagechannel->channel_num = $c->channel_num;
                        $tvpackagechannel->date = time();
                        $tvpackagechannel->contribution_id = $contribution->id;
                        R::store($tvpackagechannel);
                    }
                }
            }
        }
    }
    //Reponse
    $rep->set_data(array(
        'tvpackages' => R::getAll('
                SELECT tvpackage_id, name, path
                FROM tvpackages
                WHERE contribution_id = ?',
                [$contribution->id]
            ),
        'tvpackageschannels' => R::getAll('
                SELECT tvpackage_id, channel_id, channel_num
                FROM tvpackageschannels
                WHERE contribution_id = ?',
                [$contribution->id]
            )
        ));
    $rep->contribution_id(((!empty($rep->data['tvpackages']) || !empty($rep->data['tvpackageschannels'])) ? $contribution->id : -1));
    $app->response->setBody($rep->encode());
})->via('GET', 'POST');


$app->map('/api/image/:host/:path', function ($host,$path) use ($app) {
    if (in_array($host,[ "commons.wikimedia.org", "ar.wikipedia.org", "az.wikipedia.org", "bg.wikipedia.org", "ca.wikipedia.org", "cs.wikipedia.org", "da.wikipedia.org", "de.wikipedia.org" ,"el.wikipedia.org", "en.wikipedia.org", "es.wikipedia.org", "eo.wikipedia.org", "et.wikipedia.org", "eu.wikipedia.org", "fa.wikipedia.org", "fr.wikipedia.org", "gl.wikipedia.org", "ko.wikipedia.org", "hy.wikipedia.org", "hi.wikipedia.org", "hr.wikipedia.org", "id.wikipedia.org", "it.wikipedia.org", "he.wikipedia.org", "la.wikipedia.org", "lt.wikipedia.org", "hu.wikipedia.org", "ms.wikipedia.org", "min.wikipedia.org", "nl.wikipedia.org", "ja.wikipedia.org", "no.wikipedia.org", "nn.wikipedia.org", "uz.wikipedia.org", "pl.wikipedia.org", "pt.wikipedia.org", "kk.wikipedia.org", "ro.wikipedia.org", "ru.wikipedia.org", "sah.wikipedia.org", "simple.wikipedia.org", "ceb.wikipedia.org", "sk.wikipedia.org", "sl.wikipedia.org", "sr.wikipedia.org", "sh.wikipedia.org", "fi.wikipedia.org", "sv.wikipedia.org", "tr.wikipedia.org", "uk.wikipedia.org", "vi.wikipedia.org", "vo.wikipedia.org", "war.wikipedia.org", "zh.wikipedia.org" ]))
    {
        $path = urlencode($path);
        $args = new args();
        $width = $args->get_value('width');
        $height = $args->get_value('height');
        if (!$width && !$height)
        {
            $width = 360;
            $height = 360;
        }
        $url = "http://$host/w/api.php?action=query&titles=$path&format=json&prop=imageinfo&iiprop=url";
        if ($width) $url .= "&iiurlwidth=$width";
        if ($height) $url .= "&iiurlheight=$height";
        $rep = file_get_contents($url);
        try
        {
            //$app->redirect(array_pop(json_decode($rep,True)['query']['pages'])['imageinfo'][0]['thumburl']);
            header('Location: '.array_pop(json_decode($rep,True)['query']['pages'])['imageinfo'][0]['thumburl']);
        }
        catch(Exception $e)
        {
            $app->response->headers->set('Content-Type', 'text/plain');
            $app->response->setStatus(400);
            $app->response->setBody($e.$url."\n\n".$rep);
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
})->via('GET', 'POST');


#############
#  - GUI -  #
#############

$app->get('/', function () use ($app) {
    $user = new user();
    $app->render(
            'index.php',
            array(
                    'user'=>$user,
                )
        );

});

$app->get('/channels', function () use ($app) {
    $app->render(
            'channels.php',
            array(
                'user' => new user(),
                'grabbers' => R::getAll('
                    SELECT grabber_id, name, country
                    FROM grabbers
                    WHERE current = 1
                    ORDER BY country, name')
        ));
});


$app->get('/ajax/channels', function () use ($app) {
    $from = intval($app->request->get('from'));
    $nb = 50;
    $q_name = $app->request->get('q_name');
    $select = array();
    $left_join = array();
    $left_join_params = array();
    if ($grabber_id = $app->request->get('grabber_id'))
    {
        $select[] = 'x.xmltvid';
        $left_join[] = 'LEFT JOIN xmltvids x ON c.channel_id = x.channel_id AND x.grabber_id = ?';
        $left_join_params[] = $grabber_id;
    }
    $app->render(
            'ajax_channels.php',
            array(
                'user' => new user(),
                'channels' => R::getAll('
                    SELECT c.id, c.channel_id, c.name, GROUP_CONCAT(DISTINCT cc.country) AS country, l.url AS logo'.(!empty($select) ? ', '.join(', ',$select) : '').'
                    FROM channels c
                    LEFT JOIN alias a
                        ON c.channel_id = a.channel_id
                            AND a.accepted > 0
                    LEFT JOIN channelscountrys cc
                        ON c.channel_id = cc.channel_id
                            AND cc.accepted > 0
                    LEFT JOIN logos l
                        ON c.channel_id = l.channel_id
                            AND l.current != 0
                    '.join(' ',$left_join).'
                    WHERE c.current = 1 AND a.alias LIKE ?
                    GROUP BY c.channel_id
                    ORDER BY name
                    LIMIT ?, ?',
                    array_merge($left_join_params, [ "%$q_name%", $from, $nb]))
        ));
});


$app->get('/ajax/channels/:chan_id/edit', function ($chan_id) use ($app) {
    $args = new args();
    $select = array();
    $left_join = array();
    $left_join_params = array();
    if ($grabber_id = $args->get_value('grabber_id'))
    {
        $select[] = 'x.xmltvid';
        $left_join[] = 'LEFT JOIN xmltvids x ON c.channel_id = x.channel_id AND x.grabber_id = ?';
        $left_join_params[] = $grabber_id;
    }
    $app->render(
            'ajax_channels_edit.php',
            array(
                'channel' => R::getRow('
                    SELECT c.id, c.channel_id, c.name, GROUP_CONCAT(cc.country) AS country, l.url AS logo'.(!empty($select) ? ', '.join(', ',$select) : '').'
                    FROM channels c
                    LEFT JOIN channelscountrys cc
                        ON c.channel_id = cc.channel_id
                            AND cc.accepted > 0
                    LEFT JOIN logos l
                        ON c.channel_id = l.channel_id
                            AND l.accepted != 0
                    '.join(' ',$left_join).'
                    WHERE c.id = ?
                    GROUP BY c.channel_id
                    ',
                    array_merge($left_join_params, [ $chan_id ])
        )));
});

/*
$app->get('/ajax/wikipedia', function () use ($app) {
    $wp_lang = $app->request->get('wp_lang');
    if (!in_array($wp_lang,[ "en", "fr" ]))
    {
        $app->render('forbidden.php',array(),403);
    }
    else
    {
        $q = $app->request->get('q');
        #http://fr.wikipedia.org/wiki/Sp%C3%A9cial:ApiSandbox#action=query&list=search&format=json&srsearch=france%204&srlimit=10
        #http://en.wikipedia.org/wiki/Special%3aApiSandbox#action=query&prop=revisions&format=json&rvprop=content&rvsection=0&generator=search&gsrsearch=france%20Cha%C3%AEne%20de%20t%C3%A9l%C3%A9vision&gsrlimit=10
        $wpmrep = file_get_contents("http://$wp_lang.wikipedia/w/api.php?&action=query&generator=search&gsrnamespace=6&gsrsearch=$q&gsrlimit=20&gsroffset=0&prop=imageinfo&iiprop=url&iiurlwidth=480&iiurlheight=320&format=json");
        $app->render(
                'ajax_wikipedia.php',
                array(
                    'rep' => json_decode($wpmrep)
            ));
    }
});
*/

$app->get('/ajax/wikipedia_searchimages', function () use ($app) {
    $p = $app->request->get('p');
    if (!in_array($p,[ "commons.wikimedia.org", "ar.wikipedia.org", "az.wikipedia.org", "bg.wikipedia.org", "ca.wikipedia.org", "cs.wikipedia.org", "da.wikipedia.org", "de.wikipedia.org" ,"el.wikipedia.org", "en.wikipedia.org", "es.wikipedia.org", "eo.wikipedia.org", "et.wikipedia.org", "eu.wikipedia.org", "fa.wikipedia.org", "fr.wikipedia.org", "gl.wikipedia.org", "ko.wikipedia.org", "hy.wikipedia.org", "hi.wikipedia.org", "hr.wikipedia.org", "id.wikipedia.org", "it.wikipedia.org", "he.wikipedia.org", "la.wikipedia.org", "lt.wikipedia.org", "hu.wikipedia.org", "ms.wikipedia.org", "min.wikipedia.org", "nl.wikipedia.org", "ja.wikipedia.org", "no.wikipedia.org", "nn.wikipedia.org", "uz.wikipedia.org", "pl.wikipedia.org", "pt.wikipedia.org", "kk.wikipedia.org", "ro.wikipedia.org", "ru.wikipedia.org", "sah.wikipedia.org", "simple.wikipedia.org", "ceb.wikipedia.org", "sk.wikipedia.org", "sl.wikipedia.org", "sr.wikipedia.org", "sh.wikipedia.org", "fi.wikipedia.org", "sv.wikipedia.org", "tr.wikipedia.org", "uk.wikipedia.org", "vi.wikipedia.org", "vo.wikipedia.org", "war.wikipedia.org", "zh.wikipedia.org" ]))
    {
        $app->render('forbidden.php',array(),403);
    }
    else
    {
        $q = $app->request->get('q');
        $wpmrep = file_get_contents("http://$p/w/api.php?&action=query&generator=search&gsrnamespace=6&gsrsearch=$q&gsrlimit=20&gsroffset=0&prop=imageinfo&iiprop=url&iiurlwidth=480&iiurlheight=320&format=json");
        $app->render(
                'ajax_wikipedia_searchimages.php',
                array(
                    'p' => $p,
                    'rep' => json_decode($wpmrep)
            ));
    }
});


$app->get('/alias/unknow', function () use ($app) {
    $user = new user();
    $app->render(
            'alias_unknow.php',
            array(
                'user'=>$user,
                'alias'=>R::getAll('
                    SELECT id, alias, COUNT(*) AS nb
                    FROM aliasunknow
                    WHERE hidden = 0
                    GROUP BY alias
                    ORDER BY nb DESC
                '))
        );
});


$app->get('ajax/alias/blacklist', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        //TODO
        $app->response->setBody("TODO");
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/contributions/:id', function ($contribution_id) use ($app) {
    $contribution = R::getRow('
            SELECT id, date, email, comment, closed
            FROM contributions
            WHERE id = ?',
            [ $contribution_id ]
        );
    $contribution['name'] = parse_email($contribution['email'])['name'];
    if (!empty($contribution))
    {
        $users = array(0=>'unknow');
        foreach (R::getAll('SELECT id, name FROM users') as $u) $users[$u['id']] = $u['name'];

        $app->render(
            'contribution.php',
            array(
                    'user' => new user(),
                    'users' => $users,
                    'contribution' => $contribution,
                    'channels' => R::getAll('
                            SELECT channel_id, name, accepted, accepted_by
                            FROM channels
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'countrys' => R::getAll('
                            SELECT channel_id, country, accepted, accepted_by
                            FROM channelscountrys
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'alias' => R::getAll('
                            SELECT id, channel_id, alias, accepted, accepted_by
                            FROM alias
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'logos' => R::getAll('
                            SELECT id, channel_id, url, accepted, accepted_by
                            FROM logos
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'grabbers' => R::getAll('
                            SELECT grabber_id, name, country, web, accepted, accepted_by
                            FROM grabbers
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'xmltvids' => R::getAll('
                            SELECT channel_id, grabber_id, xmltvid, accepted, accepted_by
                            FROM xmltvids
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'tvpackages' => R::getAll('
                            SELECT tvpackage_id, name, path, accepted, accepted_by
                            FROM tvpackages
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        ),
                    'tvpackageschannels' => R::getAll('
                            SELECT channel_id, tvpackage_id, accepted, accepted_by
                            FROM tvpackageschannels
                            WHERE contribution_id = ?',
                            [ $contribution_id ]
                        )
            ));
    }
    else
    {
        $app->render('nofound.php',array(),404);
    }
});


###############
#  - LOGIN -  #
###############

$app->get('/login', function () use ($app) {
    $openid_identifier = $app->request->get('openid_identifier');
    if ($openid_identifier) {
        global $config_HypridAuth;
        require_once( "lib/Hybrid/Auth.php" );
        $ha = new Hybrid_Auth( $config_HypridAuth );
        $adapter = $ha->authenticate( "OpenID", array( "openid_identifier" => $openid_identifier));
        $profil = $adapter->getUserProfile();
        //on vérifie que ce n'est pas un utilisateur déjà enregistrer
        $userexist = R::getRow(
            'SELECT accepted FROM users WHERE openid_identifier = ?',
            [$profil->identifier]
          );
        if (empty($userexist)) {    //subscribe
            $user = R::dispense('users');
            $user->name = $profil->displayName;
            $user->email = $profil->email;
            $user->openid_identifier = $profil->identifier;
            $user->id = R::store($user);
            $_SESSION["user_connected"] = True;
            $app->redirect('profil' );
        } else {
            $_SESSION["user_connected"] = True;
            $user = new user();
            if ($user->in_group('admin')) $app->redirect('admin');
            elseif ($user->in_group('moderator')) $app->redirect('contributions/pendings');
            else $app->redirect('profil');
        }
    } else {
        $app->render('login.php');
    }
});


$app->get('/logout', function () use ($app) {
    global $config_HypridAuth;
    require_once( "lib/Hybrid/Auth.php" );
    $ha = new Hybrid_Auth( $config_HypridAuth );
    $ha->logoutAllProviders();
    $_SESSION["user_connected"] = False;
});


$app->get('/profil', function () use ($app) {
    $user = new user();
    if ($user->id != 0) { //not a guest
        $app->render(
            'profil.php',
            array('user'=>$user)
            );
    } else {
        $app->render(
            'forbidden.php',
            array(),
            403
          );
    }
});

$app->post('/profil', function () use ($app) {
    $user = new user();
    if ($user->id != 0) { //not a guest
        $newuser = R::dispense('users');
        $newuser->id = $user->id;
        $newuser->name = $app->request->post('name');
        $newuser->email = $app->request->post('email');
        R::store($newuser);
        $app->redirect('profil');
    } else {
        $app->render(
            'forbidden.php',
            array(),
            403
          );
    }
});



###############
#  - ADMIN -  #
###############


$app->get('/admin', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin')) $app->render('admin.php',array('user'=>$user));
    else $app->render('forbidden.php',array(),403);
});


$app->get('/admin/users', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin')) {
        $data = array();
        $data['users'] = R::getAll('
                SELECT id, name, email, openid_identifier
                FROM users
                WHERE accepted != 0
                ORDER BY name ASC'
            );
        $data['groups'] = array();
        $groups = R::getAll('
                SELECT id, name
                FROM groups
                ORDER BY name ASC'
            );
        foreach ($groups as $group) {
            $group['users'] = R::getCol('
                SELECT user_id
                FROM users2groups
                WHERE group_id = ?',
                [ $group['id'] ]
            );
            $data['groups'][] = $group;
        }
        $data['users_registration_pending'] = R::getAll('
                SELECT id, name, email, openid_identifier
                FROM users
                WHERE accepted = 0
                ORDER BY name ASC'
            );
        $data['user'] = $user;

        $app->render('admin_users.php',$data);
    } else {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/groups/:group_id/add_user/:user_id', function ($group_id, $user_id) use ($app) {
    $user = new user();
    if ($user->in_group('admin')) {
        R::exec('
            INSERT INTO users2groups (user_id, group_id)
            SELECT ?, ? FROM DUAL
            WHERE NOT EXISTS (  SELECT 1
                                FROM users2groups
                                WHERE user_id = ?
                                    AND group_id = ?)',
            [$user_id, $group_id, $user_id, $group_id]
          );
        $app->response->setBody(json_encode(True));
    } else {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/groups/:group_id/remove_user/:user_id', function ($group_id, $user_id) use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        R::exec('
            DELETE FROM users2groups
            WHERE user_id= ?
                AND group_id = ? ',
            [$user_id, $group_id]
          );
        $app->response->setBody(json_encode(True));
    } else {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/contributions/pendings', function () use ($app) {
    $user = new user();
    if ($user->in_group('moderator'))
    {
        $contributions = R::getAll('
                SELECT id, comment, email, date
                FROM contributions
                WHERE closed = 0
                ORDER BY id ASC'
            );
        $app->render(
                'admin_contributions.php',
                array('user'=>$user,'contributions'=>$contributions)
            );
    } else {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/tools', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        if ($app->request->get('do'))
        {
            switch ($app->request->get('do')) {
                case 'replace_channel_id' :
                    $old = $app->request->get('old');
                    $new = $app->request->get('new');
                    if (!$new)
                    {
                        $app->response->setBody('Error: new is empty');
                    }
                    elseif (R::count('channels','channel_id = ?', [ $new ]))
                    {
                        $app->response->setBody('Error: '.$new.' already existe');
                    }
                    elseif (!R::count('channels','channel_id = ?', [ $old ]))
                    {
                        $app->response->setBody('Error: '.$old.' not existe');
                    }
                    else
                    {
                        foreach (['alias', 'channels', 'channelscountrys', 'logos', 'xmltvids'] as $table)
                        {
                            R::exec('
                                    UPDATE '.$table.'
                                    SET channel_id = ?
                                    WHERE channel_id = ?',
                                    [ $new, $old ]
                                );
                        }
                        $app->response->setBody('Success: '.$old.' have be replaced by '.$new);
                    }
                    break;
                case 'replace_countrys' :
                    $channel_id = $app->request->get('channel_id');
                    if (!$channel_id)
                    {
                        $app->response->setBody('Error: channel_id require');
                    }
                    else
                    {
                        $new_countrys = explode(',',$app->request->get('countrys'));
                        $old_countrys = R::getCol('
                            SELECT country
                            FROM channelscountrys
                            WHERE channel_id = ? and accepted > 0',
                            [ $channel_id ]
                        );
                        R::exec('
                            DELETE FROM channelscountrys
                            WHERE channel_id = ? AND country IN (?)',
                            [ $channel_id, join(', ',array_diff($old_countrys,$new_countrys)) ]
                        );
                        foreach (array_diff($new_countrys, $old_countrys) as $c )
                            R::exec('
                                INSERT INTO channelscountrys (channel_id, country, date, accepted)
                                VALUES ( ?, ? , ? , ?)',
                                [ $channel_id, $c, time(), time() ]
                            );
                        $app->response->setBody('Success: new countrys recording');
                    }
                    break;
                default :
                    $app->response->setBody('unknow "do='.$app->request->get('do').'" => abord');
                    break;
            }
        }
        else
        {
            $app->render(
                    'admin_tools.php',
                    array('user'=>$user)
                );
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/tools/alias_cleaner', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        $app->render(
                    'admin_alias_cleaner.php',
                    array(
                        'user'=>$user,
                        'remplacements' => R::getAll('SELECT * FROM aliascleanerremplacements')
                    )
                );

    } else {
        $app->render('forbidden.php',array(),403);
    }
});

$app->post('/admin/tools/alias_cleaner', function () use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        if ($app->request->post('update'))
        {
            R::exec('
                UPDATE aliascleanerremplacements
                SET `search` = ?, `replace` = ?
                WHERE id = ?',
                [ $app->request->post('search'), $app->request->post('replace'), $app->request->post('id') ]
            );
        }
        elseif ($app->request->post('delete'))
        {
            R::exec('
                DELETE FROM aliascleanerremplacements
                WHERE id = ?',
                [ $app->request->post('id') ]
            );
        }
        elseif ($app->request->post('add'))
        {
            R::exec('
                INSERT INTO aliascleanerremplacements (`search`, `replace`)
                VALUES (?, ?)',
                [ $app->request->post('search'), $app->request->post('replace') ]
            );
        }
        $app->redirect('');

    } else {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/users/:id/accept', function ($user_id) use ($app) {
    $user = new user();
    if ($user->in_group('admin'))
    {
        R::exec('
                UPDATE users
                SET accepted = ?
                WHERE id= ?',
                [ time(), $user_id ]
            );
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/contributions/:id', function ($contribution_id) use ($app) {
    $user = new user();
    if ($user->in_group('moderator'))
    {
        $data['contribution'] = array(
                'id'=> $contribution_id
            );
        $data['channels'] = R::getAll('
                SELECT new.id, new.channel_id,
                    new.name AS new_name,
                    old.name AS old_name
                FROM channels new
                LEFT JOIN channels old
                    ON new.channel_id = old.channel_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0
                ORDER BY new.channel_id ASC',
                [ $contribution_id ]
            );
        $data['channelscountrys'] = R::getAll('
                SELECT id, channel_id, country
                FROM channelscountrys
                WHERE contribution_id = ?
                    AND accepted = 0
                ORDER BY channel_id ASC',
                [ $contribution_id ]
            );
        $data['alias'] = R::getAll('
                SELECT id, channel_id, alias
                FROM alias
                WHERE contribution_id = ?
                    AND accepted = 0
                ORDER BY channel_id ASC',
                [ $contribution_id ]
            );
        $data['logos'] = R::getAll('
                SELECT new.id, new.channel_id, new.url AS new_url, old.url AS old_url
                FROM logos new
                LEFT JOIN logos old
                    ON new.channel_id = old.channel_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0
                ORDER BY new.channel_id ASC',
                [ $contribution_id ]
            );
        $data['grabbers'] = R::getAll('
                SELECT new.id, new.grabber_id,
                    new.name AS new_name, new.country AS new_country,
                    old.name AS old_name, old.country AS old_country
                FROM grabbers new
                LEFT JOIN grabbers old
                    ON new.grabber_id = old.grabber_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0',
                [ $contribution_id ]
            );
        $data['xmltvids'] = R::getAll('
                SELECT new.id, new.channel_id, new.grabber_id,
                    new.xmltvid AS new_xmltvid,
                    old.xmltvid AS old_xmltvid
                FROM xmltvids new
                LEFT JOIN xmltvids old
                    ON new.channel_id = old.channel_id AND new.grabber_id = old.grabber_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0
                ORDER BY new.channel_id ASC',
                [ $contribution_id ]
            );
        $data['tvpackages'] = R::getAll('
                SELECT
                    new.id, new.tvpackage_id,
                    new.name AS new_name, new.path AS new_path,
                    old.name AS old_name, old.path AS old_path
                FROM tvpackages new
                LEFT JOIN tvpackages old
                    ON new.tvpackage_id = old.tvpackage_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0',
                [ $contribution_id ]
            );
        $data['tvpackageschannels'] = R::getAll('
                SELECT
                    new.id, new.channel_id, new.tvpackage_id,
                    new.channel_num AS new_channel_num,
                    old.channel_num AS old_channel_num
                FROM tvpackageschannels new
                LEFT JOIN tvpackageschannels old
                    ON new.tvpackage_id = old.tvpackage_id AND new.channel_id = old.channel_id AND old.current = 1
                WHERE new.contribution_id = ?
                    AND new.accepted = 0
                ORDER BY new.tvpackage_id ASC',
                [ $contribution_id ]
            );
        $app->render('admin_ajax_contribution.php',$data);
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/ajax/contributions/:id/accept', function ($id) use ($app) {
    $user = new user();
    if ($user->in_group('moderator'))
    {
        $app->response->headers->set('Content-Type', 'application/json');
        //tables channels, alias et logos
        foreach (array('channels', 'channels', 'logos') as $type)
        { //TODO optimiser requète
            foreach (R::getAll('
                    SELECT id, channel_id
                    FROM '.$type.'
                    WHERE contribution_id = ?',
                    [ $id ]
                ) as $row)
            {
                //On desactive l'ancienne entrée
                R::exec('
                        UPDATE '.$type.'
                        SET current = 0
                        WHERE channel_id = ? AND current = 1',
                        [ $row['channel_id'] ]
                    );
                //On active la nouvelle entrée
                R::exec('
                        UPDATE '.$type.'
                        SET current = 1, accepted = ?, accepted_by = ?
                        WHERE id = ?',
                        [ time(), $user->id, $row['id'] ]
                    );
            }
        }

        //tables channelscountrys, tvpackageschannels
        foreach (array('channelscountrys') as $type)
        {
            foreach (R::getAll('
                    SELECT id, channel_id
                    FROM '.$type.'
                    WHERE contribution_id = ?',
                    [ $id ]
                ) as $row)
            {
                //On active la nouvelle entrée
                R::exec('
                        UPDATE '.$type.'
                        SET accepted = ?, accepted_by = ?
                        WHERE id = ?',
                        [ time(), $user->id, $row['id'] ]
                    );
            }
        }

        //table alias
        foreach (R::getAll('
                SELECT id, channel_id, alias
                FROM alias
                WHERE contribution_id = ?',
                [ $id ]
            ) as $row)
        {
            //On active la nouvelle entrée
            R::exec('
                    UPDATE alias
                    SET accepted = ?, accepted_by = ?
                    WHERE id = ?',
                    [ time(), $user->id, $row['id'] ]
                );
            //on supprime les alias de la base alias inconus
            R::exec(
                    'DELETE from aliasunknow WHERE alias = ?',
                    [ $row['alias'] ]
                );
        }

        //table grabbers
        foreach (R::getAll('
                SELECT id, grabber_id
                FROM grabbers
                WHERE contribution_id = ?',
                [ $id ]
            ) as $row)
        {
            //On desactive l'ancienne entrée
            R::exec('
                    UPDATE grabbers
                    SET current = 0
                    WHERE grabber_id = ? AND current = 1',
                    [ $row['grabber_id'] ]
                );
            //On active la nouvelle entrée
            R::exec('
                    UPDATE grabbers
                    SET current = 1, accepted = ?, accepted_by = ?
                    WHERE id = ?',
                    [ time(), $user->id, $row['id'] ]
                );
        }

        //table xmltvids
        foreach (R::getAll('
                SELECT id, channel_id, grabber_id
                FROM xmltvids
                WHERE contribution_id = ?',
                [ $id ]
            ) as $row)
        {
            //On desactive l'ancienne entrée
            R::exec('
                    UPDATE xmltvids
                    SET current = 0
                    WHERE grabber_id = ?  AND channel_id = ? AND current = 1',
                    [ $row['grabber_id'], $row['channel_id'] ]
                );
            //On active la nouvelle entrée
            R::exec('
                    UPDATE xmltvids
                    SET current = 1, accepted = ?, accepted_by = ?
                    WHERE id = ?',
                    [ time(), $user->id, $row['id'] ]
                );
        }

        //table tvpackages
        foreach (R::getAll('
                SELECT id, tvpackage_id, path
                FROM tvpackages
                WHERE contribution_id = ?',
                [ $id ]
            ) as $row)
        {
            $oldRow = R::getRow('
                    SELECT id, parent, path
                    FROM tvpackages
                    WHERE tvpackage_id = ? AND current = 1',
                    [ $row['tvpackage_id'] ]
                );
            if ((!$oldRow) || ($oldRow['path'] != $row['path']))
            {
                //le chamin est différent
                $parent = tvpackages_path_bulider($row['path'], $oldRow['parent']);
            }
            else
            {
                //le chemin est identique on récupère l'ancien
                $parent = $oldRow['parent'];
            }

            //On desactive l'ancienne entrée
            R::exec('
                    UPDATE tvpackages
                    SET current = 0
                    WHERE tvpackage_id = ? AND current = 1',
                    [ $row['tvpackage_id'] ]
                );
            //On active la nouvelle entrée
            R::exec('
                    UPDATE tvpackages
                    SET current = 1, accepted = ?, accepted_by = ?, parent = ?
                    WHERE id = ?',
                    [ time(), $user->id, $parent, $row['id'] ]
                );
        }

        //table tvpackageschannels
        foreach (R::getAll('
                SELECT id, channel_id, tvpackage_id
                FROM tvpackageschannels
                WHERE contribution_id = ?',
                [ $id ]
            ) as $row)
        {
            //On desactive l'ancienne entrée
            R::exec('
                    UPDATE tvpackageschannels
                    SET current = 0
                    WHERE tvpackage_id = ?  AND channel_id = ? AND current = 1',
                    [ $row['tvpackage_id'], $row['channel_id'] ]
                );
            //On active la nouvelle entrée
            R::exec('
                    UPDATE tvpackageschannels
                    SET current = 1, accepted = ?, accepted_by = ?
                    WHERE id = ?',
                    [ time(), $user->id, $row['id'] ]
                );
        }

        //On clos la contribution
        contribution_close($id);
        $app->response->setBody(json_encode(array('contribution_id'=>$id,'closed'=>True)));
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/ajax/contributions/:id/refused', function ($id) use ($app) {
    $user = new user();
    if ($user->in_group('moderator'))
    {
        $app->response->headers->set('Content-Type', 'application/json');
        //on rend a nouveau visible les alias inconnus
        $aliasunknows = R::getCol('
                SELECT alias
                FROM alias
                WHERE contribution_id = ?',
                [ $id ]
            );
        if (!empty($aliasunknows)) R::exec('
                UPDATE aliasunknow
                SET hidden = 0
                WHERE alias in ( ? )',
                [ join(', ',$aliasunknows) ]
            );
        foreach (array('channels', 'channelscountrys', 'alias', 'logos', 'grabbers', 'xmltvids') as $type)
        {
            $ids = R::getCol('
                    SELECT id
                    FROM '.$type.'
                    WHERE contribution_id = ?',
                    [ $id ]
                );
            if (!empty($ids)) R::exec('
                    DELETE FROM '.$type.'
                    WHERE id = '.join(' OR id = ',$ids).' AND accepted = 0'
                );
        }
        contribution_close($id);
        $app->response->setBody(json_encode(array('contribution_id'=>$id,'closed'=>True)));
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/channels/:channel_id/delete', function ($channel_id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator') )
    {
        $contribution = new contribution('Delete channel '.$channel_id, False,$user->name.' <'.$user->email.'>');
        contribution_close($contribution->id);
        R::exec('
                UPDATE channels
                SET current = 0
                WHERE channel_id = ?',
                [ $channel_id ]
            );
        R::exec('
                DELETE FROM alias
                WHERE channel_id = ?',
                [ $channel_id ]
            );
        $app->response->setBody("$channel_id have be deleted");
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/alias/:id/accept', function ($id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator') )
    {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, alias
                FROM alias
                WHERE id = ?
                LIMIT 1',
                [ $id ]
            );
        //on supprime l'alias de la base alias inconus
        R::exec(
                'DELETE from aliasunknow WHERE alias = ?',
                [ $row['alias'] ]
            );
        R::exec('
                UPDATE alias
                SET accepted = ?, accepted_by = ?
                WHERE id = ?',
                [ time(), $user->id, $id ]
            );

        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        } else {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/grabbers/:id/accept', function ($id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator')) {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, grabber_id
                FROM grabbers
                WHERE id = ?
                LIMIT 1',
                [ $id ]
            );
        //on désactive l'ancienne entrée
        R::exec('
                UPDATE grabbers
                SET current = 0
                WHERE grabber_id = ? AND current = 1',
                [ $row['grabber_id'] ]
            );
        //on active la nouvelle entrée
        R::exec('
                UPDATE grabbers
                SET current = 1, accepted = ?, accepted_by = ?
                WHERE id = ?',
                [ time(), $user->id, $id ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            //Si plus de contribution on clos la contribution
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/xmltvids/:id/accept', function ($id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator')) {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, grabber_id, channel_id
                FROM xmltvids
                WHERE id = ?
                LIMIT 1',
                [ $id ]
            );
        //on désactive l'ancienne entrée
        R::exec('
                UPDATE xmltvids
                SET current = 0
                WHERE grabber_id = ? AND channel_id = ? AND current = 1',
                [ $row['grabber_id'], $row['channel_id'] ]
            );
        //on active la nouvelle entrée
        R::exec('
                UPDATE xmltvids
                SET current = 1, accepted = ?, accepted_by = ?
                WHERE id = ?',
                [ time(), $user->id, $id ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            //Si plus de contribution on clos la contribution
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/tvpackages/:id/accept', function ($id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator')) {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, tvpackage_id
                FROM tvpackages
                WHERE id = ?
                LIMIT 1',
                [ $id ]
            );
        $oldRow = R::getRow('
                SELECT id, parent, path
                FROM tvpackages
                WHERE tvpackage_id = ? AND current = 1',
                [ $row['tvpackage_id'] ]
            );
        if ((!$oldRow) || ($oldRow['path'] != $row['path']))
        {
            //le chamin est différent
            $parent = tvpackages_path_bulider($row['path'], $oldRow['parent']);
        }
        else
        {
            //le chemin est identique on récupère l'ancien
            $parent = $oldRow['parent'];
        }
        //on désactive l'ancienne entrée
        R::exec('
                UPDATE tvpackages
                SET current = 0
                WHERE tvpackages_id = ? AND current = 1',
                [ $row['tvpackage_id'] ]
            );
        //on active la nouvelle entrée
        R::exec('
                UPDATE tvpackages
                SET current = 1, accepted = ?, accepted_by = ?
                WHERE id = ?',
                [ time(), $user->id, $id ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            //Si plus de contribution on clos la contribution
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/tvpackageschannels/:id/accept', function ($id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator')) {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, tvpackage_id, channel_id
                FROM tvpackageschannels
                WHERE id = ?
                LIMIT 1',
                [ $id ]
            );
        //on désactive l'ancienne entrée
        R::exec('
                UPDATE tvpackageschannels
                SET current = 0
                WHERE tvpackage_id = ? AND channel_id = ? AND current = 1',
                [ $row['tvpackage_id'], $row['channel_id'] ]
            );
        //on active la nouvelle entrée
        R::exec('
                UPDATE tvpackageschannels
                SET current = 1, accepted = ?, accepted_by = ?
                WHERE id = ?',
                [ time(), $user->id, $id ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            //Si plus de contribution on clos la contribution
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/:type/:id/accept', function ($type, $id) use ($app) {
    $user = new user();

    if ($user->in_group('moderator'))
    {
        if (in_array($type, array('channels', 'logos', 'grabbers')))
        {
            $app->response->headers->set('Content-Type', 'application/json');
            $row = R::getRow('
                    SELECT contribution_id, channel_id
                    FROM '.$type.'
                    WHERE id = ? ',
                    [ $id ]
                );
            //on désactive l'ancienne entrée
            R::exec('
                    UPDATE '.$type.'
                    SET current = 0
                    WHERE channel_id = ? AND current = 1',
                    [ $row['channel_id'] ]
                );
            //on active la nouvelle entrée
            R::exec('
                    UPDATE '.$type.'
                    SET current = 1, accepted = ?, accepted_by = ?
                    WHERE id = ?',
                    [ time(), $user->id, $id ]
                );
        }
        else
        {
            $app->render('forbidden.php',array(),403);
        }
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        } else {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});

$app->get('/admin/ajax/alias/:id/refused', function ($id) use ($app) {
    $user = new user();
    if ($user->in_group('moderator'))
    {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id, alias
                FROM alias
                WHERE id = ?',
                [ $id ]
            );
        R::exec('
                DELETE FROM alias
                WHERE id = ? ',
                [ $id ]
            );
        //on rend a nouveau visible l'alias inconnus
        R::exec('
                UPDATE aliasunknow
                SET hidden = 0
                WHERE alias = ?',
                [ $row['alias'] ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


$app->get('/admin/ajax/:type/:id/refused', function ($type, $id) use ($app) {
    $user = new user();
    if ($user->in_group('moderator') && in_array($type, ['channels','channelscountrys', 'logos', 'grabbers', 'xmltvids', 'tvpackages', 'tvpackageschannels']))
    {
        $app->response->headers->set('Content-Type', 'application/json');
        $row = R::getRow('
                SELECT contribution_id
                FROM '.$type.'
                WHERE id = ? ',
                [ $id ]
            );
        R::exec('
                DELETE FROM '.$type.'
                WHERE id = ? AND accepted = 0',
                [ $id ]
            );
        if ( array_sum(contribution_count_entrys($row['contribution_id'])) == 0)
        {
            contribution_close($row['contribution_id']);
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>True)));
        }
        else
        {
            $app->response->setBody(json_encode(array('contribution_id'=>$row['contribution_id'],'closed'=>False)));
        }
    }
    else
    {
        $app->render('forbidden.php',array(),403);
    }
});


################
#  - SYSTEM -  #
################

$app->map('/auth', function () {
    require_once( "lib/Hybrid/Auth.php" );
    require_once( "lib/Hybrid/Endpoint.php" );
    Hybrid_Endpoint::process();
})->via('GET', 'POST');

//~ $app->map('/proxy', function () use ($app) {
    //~ if (!isset($_GET['url'])) die();
    //~ $url = urldecode($_GET['url']);
    //~ $url = 'http://' . str_replace('http://', '', $url); // Avoid accessing the file system
    //~ $app->response->setBody(file_get_contents($url));
//~ })->via('GET', 'POST');

$app->run();

