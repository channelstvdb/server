SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `alias` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL,
  `date` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `aliascleanerremplacements` (
`id` int(11) unsigned NOT NULL,
  `search` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `replace` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `aliasunknow` (
`id` int(11) unsigned NOT NULL,
  `alias` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `date` int(11) unsigned NOT NULL,
  `ip` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `channels` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `channelscountrys` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL,
  `date` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `contributions` (
`id` int(11) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `closed` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `grabbers` (
`id` int(11) unsigned NOT NULL,
  `grabber_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `logos` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tvpackages` (
`id` int(11) unsigned NOT NULL,
  `parent` int(11) NOT NULL,
  `tvpackage_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` int(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `tvpackageschannels` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tvpackage_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_num` int(1) unsigned DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` tinyint(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE IF NOT EXISTS `tvpackagespaths` (
`id` int(11) unsigned NOT NULL,
  `parent` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `openid_identifier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accepted` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users2groups` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `xmltvids` (
`id` int(11) unsigned NOT NULL,
  `channel_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grabber_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `xmltvid` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `contribution_id` tinyint(11) unsigned DEFAULT NULL,
  `current` tinyint(1) NOT NULL,
  `accepted` int(11) unsigned NOT NULL,
  `accepted_by` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `alias`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `alias` (`alias`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `accepted` (`accepted`);

ALTER TABLE `aliascleanerremplacements`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `aliasunknow`
 ADD PRIMARY KEY (`id`), ADD KEY `alias` (`alias`), ADD KEY `hidden` (`hidden`), ADD KEY `ip` (`ip`);

ALTER TABLE `channels`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `name` (`name`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `current` (`current`);

ALTER TABLE `channelscountrys`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `country` (`country`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `accepted` (`accepted`);

ALTER TABLE `contributions`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `grabbers`
 ADD PRIMARY KEY (`id`), ADD KEY `grabber_id` (`grabber_id`), ADD KEY `name` (`name`), ADD KEY `country` (`country`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `current` (`current`);

ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `logos`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `current` (`current`);

ALTER TABLE `tvpackages`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`), ADD KEY `contribution_id` (`contribution_id`), ADD KEY `current` (`current`), ADD KEY `tvpackage_id` (`tvpackage_id`), ADD KEY `accepted` (`accepted`), ADD KEY `path` (`path`(191));

ALTER TABLE `tvpackageschannels`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `tvpackage_id` (`tvpackage_id`), ADD KEY `current` (`current`);

ALTER TABLE `tvpackagespaths`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`), ADD KEY `parent` (`parent`);

ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `openid_identifier` (`openid_identifier`(191));

ALTER TABLE `users2groups`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `group_id` (`group_id`);

ALTER TABLE `xmltvids`
 ADD PRIMARY KEY (`id`), ADD KEY `channel_id` (`channel_id`), ADD KEY `grabber_id` (`grabber_id`), ADD KEY `xmltvid` (`xmltvid`), ADD KEY `current` (`current`);


ALTER TABLE `alias`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `aliascleanerremplacements`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `aliasunknow`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `channels`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `channelscountrys`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `contributions`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `grabbers`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `groups`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `logos`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `tvpackages`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `tvpackageschannels`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `tvpackagespaths`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `users2groups`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `xmltvids`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;


INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'moderator'),
(3, 'contributor');

INSERT INTO `users2groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
