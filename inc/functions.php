<?php

class args
{
    var $format = 'json';
    var $countrys = array('*');

    function __construct($comment='')
    {
        global $app, $rep, $countrys;

        if ($this->get_value('format')) $this->format = $this->get_value('format');

        if ($this->get_value('countrys')) $this->countrys = explode(',', strtolower($this->get_value('countrys'))) ;
        elseif ($this->get_value('country')) $this->countrys = explode(',', strtolower($this->get_value('country')));
    }

    function get_value($opt)
    {
        global $app, $rep;

        //~ if (($value = $app->request->getBody($opt)))
            //~ $rep->debug('opt "'.$opt.'": '.$value.' (input by BODY, ignore POST and GET)');
        //~ elseif (($value = $app->request->post($opt)))
        if (($value = $app->request->post($opt)))
            $rep->debug('opt "'.$opt.'": '.$value.' (input by POST, ignore GET)');
        elseif (($value = $app->request->get($opt)))
            $rep->debug('opt "'.$opt.'": '.$value.' (input by GET)');
        else
        {
            $rep->debug('opt "'.$opt.'" unspecified');
            return False;
        }
        return $value;
    }

    function get_json_values($opt) {
        global $rep;

        $json = json_decode($this->get_value($opt));

        if ($json) {
            return $json;
        }
        else
        {
            $rep->error('Json decode error: '.json_last_error_msg());
            return array();
        }
    }
}


class rep {
    var $errors = array();
    var $warnings = array();
    var $infos = array();
    var $debugs = array();
    var $data = array();
    var $format = 'json';

    function error ($str)       { $this->errors[] = $str ;}
    function warning ($str)     { $this->warnings[] = $str ;}
    function info ($str)        { $this->infos[] = $str ;}
    function debug ($str)       { if (DEBUG) { ob_start(); var_dump($str); $this->debugs[] = ob_get_contents(); ob_end_clean();} }
    function add_data ($str)    { $this->data[] = $str ;}
    function set_data ($str)    { $this->data = $str ;}
    function contribution_id ($int) { $this->contribution_id = $int ;}

    function encode ()
    {
        global $args, $app;
        $ret = array();
        if (empty($this->data))             $this->infos[] = 'No result';
        if (!empty($this->errors))          $ret['errors']=$this->errors ;
        if (!empty($this->warnings))        $ret['warnings']=$this->warnings ;
        if (!empty($this->infos))           $ret['infos']=$this->infos ;
        if (!empty($this->contribution_id)) $ret['contribution_id']=$this->contribution_id ;
        $ret['data']=$this->data;
        if (DEBUG) $ret['debugs']=$this->debugs;
        if (DEBUG) $ret['debugs']['RedBeam'] = R::getDatabaseAdapter()->getDatabase()->getLogger()->log();

        switch ($args->format)
        {
            case 'json' :
                $app->response->headers->set('Content-Type', 'application/json');
                return json_encode($ret);
                break;
            case 'txt' :
                return var_dump($ret);
                break;
            case 'php' :
                return serialize($ret);
                break;
            case 'html' :
                function arraytohtml($arr)
                {
                    if (is_array($arr))
                    {
                        $ret = '<ul>';
                        foreach ($arr as $k => $v)
                        {
                            if (is_array($v)) $ret .= '<li><code>'.$k.arraytohtml($v).'</code></li>';
                            else $ret .= '<li><code>'.$k.': '.$v.'</code></li>';
                        }
                        return $ret.'</ul>';
                    }
                    else
                    {
                        return '<p style="padding-left:1em"><code>empty</code></p>';
                    }
                }

                return '<html>'.
                    (isset($ret['contribution_id']) ? '
                    <div style="color:black">
                        <p><a href="'.SLIM_URL.'contributions/'.$ret['contribution_id'].'">Contribution #'.$ret['contribution_id'].'</a></p>
                        '.arraytohtml($ret['data']).'
                    </div> ' : '').'
                    <div style="color:blue">
                        <p>data</p>
                        '.arraytohtml($ret['data']).'
                    </div>
                    <div style="color:red">
                        <span >errors</span>
                        '.(isset($ret['errors']) ? arraytohtml($ret['errors']) : '<p style="padding-left:1em"><code>empty</code></p>').'
                    </div>
                    <div style="color:orange">
                        <span>warnings</span>
                        '.(isset($ret['warnings']) ? arraytohtml($ret['warnings']) : '<p style="padding-left:1em"><code>empty</code></p>').'
                    </div>
                    <div style="color:green">
                        <span>infos</span>
                        '.(isset($ret['infos']) ? arraytohtml($ret['infos']) : '<p style="padding-left:1em"><code>empty</code></p>').'
                    </div>
                    <div style="color:gray">
                        <span>debugs</span>
                        '.(isset($ret['debugs']) ? arraytohtml($ret['debugs']) : '<p style="padding-left:1em"><code>empty</code></p>').'
                    </div>
                </html>';
                break;
            default :
                return 'Error: unknow format '.$args->format;
                break;
        }
    }
}

class user {
    var $id = 0;
    var $name = 'Guest';
    var $email = '';
    var $emailRFC = '';
    var $identifier = '';
    var $groups = array();

    function __construct()
    {
        global $config_HypridAuth, $app;
        if (isset($_SESSION["user_connected"]) && $_SESSION["user_connected"] == True)
        {
            try
            {
                require_once( "lib/Hybrid/Auth.php" );
                $ha = new Hybrid_Auth( $config_HypridAuth );
                $adapter = $ha->authenticate('OpenID');
                $this->openid_identifier = $adapter->getUserProfile()->identifier;
            }
            catch( Exception $e )
            {
                echo "Error: please try again!";
                echo "Original error message: " . $e->getMessage();
            }
            $this->openid_identifier = $adapter->getUserProfile()->identifier;
            $this->groups = array();
            $user = R::getRow('
                SELECT id, name, email, accepted
                FROM users u
                WHERE u.openid_identifier = ?
                LIMIT 1',
                [ $this->openid_identifier ]
              );
            $this->id = $user['id'];
            $this->name = $user['name'];
            $this->email = $user['email'];
            $this->emailRFC = $user['name'].' <'.$user['email'].'>';
            $this->accepted = $user['accepted'];
            foreach (R::getAll('
                    SELECT g.id, g.name
                    FROM users2groups u2g
                    LEFT JOIN groups g ON u2g.group_id = g.id
                    WHERE u2g.user_id = ?',
                    [ $this->id ]
                ) as $group )
            {
                $this->groups[$group['id']] = $group['name'];
            }
        }
        if (isset($_SESSION["emailRFC"]) && $_SESSION["emailRFC"])
        {
            $this->emailRFC = $_SESSION["emailRFC"];
        }
    }

    function in_group($group)
    {
        if (!isset($this->accepted))    { $_SESSION["user_connected"] = false; return false; }
        elseif (!$this->accepted)       { return false; }
        elseif (is_numeric($group))     { return array_key_exists(intval($group),$this->groups); }
        else                            { return in_array($group,$this->groups); }
    }

    function is_guest()
    {
        if ($this->id == 0) return True;
        else return False;
    }
}

class contribution
{
    function __construct($comment='',$removeIfIsEmpty=True, $email='')
    {
        global $app, $rep, $args;
        $this->removeIfIsEmpty = $removeIfIsEmpty;
        $email = $email ? $email : ($args->get_value('email') ? $args->get_value('email') : '');
        $_SESSION["emailRFC"] = $email;
        if ($this->id = R::getCell('
                SELECT id
                FROM contributions
                WHERE ip = ? AND email = ? AND date > ?',
                [ $_SERVER["REMOTE_ADDR"], $email, time() - 2700 /*15 minutes*/ ]
            ))
        {
            //une autre contribution à été effectuée il y a moin de 15 minutes avec la même ip et le même mail => considéré comme une même et unique contribution
            R::exec('
                    UPDATE contributions
                    SET date = ?, closed = 0
                    WHERE id = ?',
                    [ time(), $this->id ]
                );
        }
        else
        {
            $contribution = R::dispense('contributions');
            $contribution->email = $email ? $email : ($args->get_value('email') ? $args->get_value('email') : '');
            $contribution->ip = $_SERVER["REMOTE_ADDR"];
            $contribution->comment = $comment;
            $contribution->date = time();
            $this->id = R::store($contribution);
            $rep->debug('id contribution: '.$this->id);
        }
    }

    function __destruct()
    {
        if ( $this->removeIfIsEmpty && array_sum(contribution_count_entrys($this->id)) == 0 )
        {
            R::exec('
                    DELETE FROM contributions
                    WHERE id = ?',
                    [ $this->id ]
                );
            R::exec('
                    ALTER TABLE contributions AUTO_INCREMENT = 1'
                );
        }
    }
}

function contribution_count_entrys($id_contribution)
{
    return R::getRow('
            SELECT *
            FROM
                (SELECT COUNT(*) AS nb_alias FROM alias         WHERE contribution_id = ? AND accepted = 0) as a,
                (SELECT COUNT(*) AS nb_channels FROM channels   WHERE contribution_id = ? AND accepted = 0) as c,
                (SELECT COUNT(*) AS nb_channelscountrys FROM channelscountrys WHERE contribution_id = ? AND accepted = 0) as cc,
                (SELECT COUNT(*) AS nb_logos FROM logos         WHERE contribution_id = ? AND accepted = 0) as l,
                (SELECT COUNT(*) AS nb_grabbers FROM grabbers   WHERE contribution_id = ? AND accepted = 0) as g,
                (SELECT COUNT(*) AS nb_xmltvids FROM xmltvids   WHERE contribution_id = ? AND accepted = 0) as x,
                (SELECT COUNT(*) AS nb_tvpackages FROM tvpackages WHERE contribution_id = ? AND accepted = 0) as tvp,
                (SELECT COUNT(*) AS nb_tvpackageschannels FROM tvpackageschannels   WHERE contribution_id = ? AND accepted = 0) as tvpc',
            [ $id_contribution, $id_contribution, $id_contribution, $id_contribution, $id_contribution, $id_contribution, $id_contribution, $id_contribution ]
        );
}

function contribution_close($id_contribution)
{
    R::exec('
            UPDATE contributions
            SET closed = ?
            WHERE id = ?',
            [ time(), $id_contribution ]
        );


    if ($email = R::getCell('
            SELECT email
            FROM contributions
            WHERE ?',
            [ $id_contribution ]
        ))
    {
        email($email,"Contribution #$id_contribution closed", "Contribution #$id_contribution have be closed");
    }
}

function is_valid_id ($str)
{
    if (strlen($str)>36) return False;
    else return preg_match("/^[a-z0-9\._-]*$/",$str);
}

class alias_cleaner
{
    function __construct()
    {
        $this->remplacements = R::getAll('SELECT `search`, `replace` FROM aliascleanerremplacements');
    }

    function clean ($str)
    {
        foreach ($this->remplacements as $r)
           $str = str_replace($r['search'], $r['replace'], $str);
        return $str;
    }
}

function tvpackages_path_bulider($newpath, $previous_parent = False)
{
    //On supprime l'ancien chemin s'il n'est pas utilisé par un autre élément
    $parent = $previous_parent;
    while (
            $parent
            && !R::count('tvpackages', 'parent = ? AND current = 1', [ $parent ])
            && !R::count('tvpackagespaths', 'parent = ?', [ $parent ]
        ))
    {
        $parent = R::getCell('
                SELECT parent FROM tvpackagespaths WHERE id = ?;
                DELETE FROM tvpackagespaths WHERE id = ?',
                [ $parent, $parent ]
            );
    }
    //On creer le nouveau chemin
    $parent = 0;
    foreach (json_decode($newpath) as $part)
    {
        $id = R::getCell('SELECT id FROM tvpackagespaths WHERE name = ? AND parent = ?', [ $part, $parent ]);
        if (!$id)
        {
            $tvpackagespaths = R::dispense('tvpackagespaths');
            $tvpackagespaths->name = $part;
            $tvpackagespaths->parent = $parent;
            R::store($tvpackagespaths);
            $id = $tvpackagespaths->id;
        }
        $parent = $id;
    }
    return $parent;
}

class image
{
    function __construct($path,$width=0,$height=0)
    {
        if (!$path)
        {
            $this->name = '';
            $this->url = '';
        }
        elseif (strpos($path, SLIM_URL) === 0)
        {
            $this->name = basename($path);
            $query = [];
            if ($width) $query['width'] = $width;
            if ($height) $query['height'] = $height;
            $this->url = $path.'?'.http_build_query($query);
        }
        else
        {
            ;
            $this->name = basename($path);
            $this->url = $path;
        }
    }

    function get_htmltag ($params= [], $ifempty='')
    {
        if (!$this->url && $ifempty)
            return $ifempty;
        $r = '<img ';
        foreach (array_merge(['src'=>$this->url, 'alt'=>$this->name],$params) as $pname=>$pvalue)
            $r .= htmlspecialchars($pname).'="'.htmlspecialchars($pvalue).'" ';
        $r .= ' />';
        return $r;
    }
}

function parse_email($e) {
    @list($name, $mail) = explode('<', trim($e,'>'),2);
    if (!trim($mail)) $mail = $name;
    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $mail = '';
    $name = explode('@', $name,2)[0];
    if (!$name) $name='unknow';
    return [ 'name' => trim($name), 'email' => trim($mail)];
}

function email($to,$subject,$message,$from=false) {
    global $config_mail;
    if ($to = parse_email($to)['email'])
    {
        if (!$from) $from = $config_mail['default_from'];
        mail(
                $to,
                $config_mail['prefix_subject'].$subject,
                $config_mail['prefix_message'].$message,
                "From:".$from
            );
    }
}

?>
