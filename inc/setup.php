<?php
define('DEBUG', False);

define('HTTP_HOST','http://localhost');
define('BASE_URL', 'http://localhost/Web/api/mythChannels/');
define('SLIM_URL', 'http://localhost/Web/api/mythChannels/index.php/');

//Slim Framwork
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(array(
    'mode' => (DEBUG ? 'development' : 'production')
    ));

//RedBeam
R::setup('mysql:host=localhost;dbname=channelsdbtv','channelsdbtv','channelsdbtv');

//HybridAuth
$config_HypridAuth = array(
    "base_url" => SLIM_URL.'auth',
    "providers" => array ( "OpenID" => array ( "enabled" => true ) )
    );

//sendmail
$config_mail = array(
    "default_from" => "noreponse@channelstvdb.mythtv-fr.org",
    "prefix_subject" => "[ChannelsTVDB] ",
    "prefix_message" => "-------------------------\nThis is an automated message toto.\nYou received this message because you have subscribed you to the notification by email.\n-------------------------\n\n"
);
?>
