<?php
if (!isset($this)) exit(1);

foreach ($channels as $channel) {
    $logo = new image($channel['logo'],100,60);
?>
    <tr class="row_channel" id="row_<?php echo $channel['id'] ?>">
        <td><?php
            echo '<span>'.$channel['channel_id'].'</span>';
            if ($user->in_group('admin')) echo '</sup><a class="bt_rename admin_fct" channel_id="'.$channel['channel_id'].'" href="#">[rename]</a>'
        ?></td>
        <td><?php echo $logo->get_htmltag(['class'=>'logo'],'no logo') ?></td>
        <td><?php echo $channel['name'] ?></td>
        <td><?php
            echo $channel['country'];
            if ($user->in_group('admin')) echo '<a class="bt_change_countrys admin_fct" channel_id="'.$channel['channel_id'].'" href="#">[edit]</a>'
        ?></td>
        <td><?php echo (isset($channel['xmltvid']) && $channel['xmltvid']) ? $channel['xmltvid'] : '-' ?></td>
        <td>
            <a class="bt_edit" channel_id="<?php echo $channel['id'] ?>" href="#">[edit]</a>
<?php if ($user->in_group('admin')) echo '
            <a class="bt_remove" channel_id="'.$channel['channel_id'].'" href="#">[delete]</a>
            <a class="bt_merge admin_fct" href="#">[merge]</a>
'; ?>
        </td>
    </tr>
<?php
}
?>
