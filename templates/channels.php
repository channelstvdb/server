<?php
if (!isset($this)) exit(1);

$title = 'Channels';
$add_header = '
<link type="text/css" rel="stylesheet" href="'.BASE_URL.'/css/jquery-ui-1.10.4.custom.css" />
<script type="text/javascript" src="'.BASE_URL.'js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="'.BASE_URL.'js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript">
    var inLoad = false;
    var allLoad = false;
    var callbackLoad;
    function append_channels () {
        var $window = $(window);
        var $channels = $("#channels");
        var $q_name = $("#q_name");
        var $grabber_id = $("#grabber_id");
        var $load = $(".load");
        clearTimeout(callbackLoad);
        if (allLoad==false && ($channels.height() + $channels.offset().top - $window.height() - $window.scrollTop() < 100)) {
            $load.show();
            if (inLoad) {
                callbackLoad = setTimeout("append_channels()",500);
            }
            else
            {
                inLoad = true;
                $.ajax({
                    url: "'.SLIM_URL.'ajax/channels",
                    type: "get",
                    data: {
                        "from": $channels.children().length,
                        "q_name": $q_name.val(),
                        "grabber_id": $grabber_id.val()
                    },
                    success: function(data) {
                        inLoad = false;
                        allLoad = true
                        $channels.append(data);
                        if ($(data).length){
                            allLoad = false;
                            append_channels ();
                        }
                        $(".bt_edit").click(function(){
                            $propose_change.show("highlight",500);
                            var $bt = $( this );
                            chan_id = $bt.attr("channel_id");
                            var $row = $bt.parent().parent();
                            $row.addClass("row_editable_channel");
                            $row.load("'.SLIM_URL.'ajax/channels/"+chan_id+"/edit'.((isset($this->data->grabber) && $this->data->grabber) ? '?grabber='.$this->data->grabber : '').'", function(){
                                $(this).find(".wp_logo_dialog").hide();
                                $(this).find(".wp_logo_bt_dialog_toggle").click(function(){
                                    $parent = $(this).parent();
                                    $parent.find(".wp_logo_dialog").toggle("blur");
                                    $parent.find(".wp_logo_query").val($parent.parent().find(".channel_name").val());
                                    $parent.find(".wp_logo_provider").val(wp_logo_provider);
                                    return false;
                                });
                                $(this).find(".wp_logo_bt_load").click(function(){
                                    parent = $(this).parent();
                                    console.log(parent);
                                    parent.find(".logo_img").attr("src",parent.find(".logo_url").val());
                                    return false;
                                })
                                $(this).find(".wp_logo_bt").click(function(){
                                    $parent = $(this).parent()
                                    wp_logo_provider = $parent.find(".wp_logo_provider").val();
                                    $parent.find(".wp_logo_result").load("'.SLIM_URL.'ajax/wikipedia_searchimages?q=" + $parent.find(".wp_logo_query").val().replace(/ /g,"_") + "&p=" + wp_logo_provider,function(){
                                        $(this).find("img").click(function(){
                                            $parent = $(this).parent().parent().parent().parent();
                                            $parent.find(".logo_url").val($(this).attr("path"));
                                            $parent.find(".logo_img").attr("src",$(this).attr("src"));
                                            $parent.find(".wp_logo_dialog").hide("blur");
                                        });
                                    });
                                    return false;
                                });
                            });
                            return false;
                        });
'.
($user->in_group('admin')
    ? '
                        $(".bt_rename").click(function() {
                            var $this = $(this);
                            var old_channel_id = $this.attr("channel_id");
                            var new_channel_id = prompt("New channel_id:",old_channel_id);
                            if (new_channel_id) {
                                $this = $(this);
                                $.ajax({
                                    url :  "'.SLIM_URL.'admin/tools",
                                    data : {
                                        do: "replace_channel_id",
                                        old:old_channel_id,
                                        new: new_channel_id
                                    },
                                    success : function(data) {
                                        console.log(data);
                                        if (data.indexOf("Success")==0) {
                                            console.log("Success");
                                            console.log($this.parent().text());
                                            $this.parent().find("span").text(new_channel_id);
                                            $this.parent().parent().find("[channel_id]").attr("channel_id",new_channel_id);
                                        }
                                        alert(data);
                                    }
                                });
                            }
                            return false;
                        });
                        $(".bt_change_countrys").click(function() {
                            var $this = $(this);
                            var channel_id = $this.attr("channel_id")
                            $.ajax({
                                url :  "'.SLIM_URL.'api/get_countrys",
                                data : {channel_id : channel_id},
                                success : function(data) {
                                    console.log(data);
                                    try {
                                        new_countrys = prompt("list of countrys:",data["data"]["countrys"].join(","));
                                    } catch(e) {
                                        new_countrys = prompt("list of countrys:");
                                    }
                                    if (new_countrys!=null) {
                                        $.ajax({
                                            url :  "'.SLIM_URL.'admin/tools",
                                            data : {
                                                do: "replace_countrys",
                                                channel_id:channel_id,
                                                countrys: new_countrys
                                            },
                                            success : function(data) {
                                                console.log(data);
                                                alert(data+"\nFor view changes reload page.");
                                            }
                                        });
                                    }
                                }
                            });
                            return false;
                        });
                        $(".bt_merge").click(function() {
                            $row = $(this).parent().parent();
                            $merge_row = $row.clone();
                            $merge_table = $("#merge_table")
                            $row.hide();
                            $merge_row.attr("old_id",$merge_row.attr("id"));
                            $merge_row.removeAttr("id");
                            channel_id = $merge_row.children().eq(0).find("span").text();
                            //base
                            $merge_base = $("<input type=\"radio\" name=\"merge_base\" group=\"merge_base\" />");
                            $merge_base.val(channel_id);
                            $merge_row.prepend($("<td />").append($merge_base));
                            //channel_id
                            $merge_row.children().eq(1).find("a").remove();
                            $merge_row.children().eq(1).click(function(){$("#merge_channel_id").val($(this).parent().find("span").text())});
                            //logo
                            $merge_row.children().eq(2).click(function(){$("#merge_logo").val($(this).parent().find("img").attr("src").split("?")[0])});
                            //name
                            $merge_row.children().eq(3).click(function(){$("#merge_name").val($(this).text())});
                            //country
                            $merge_row.children().eq(4).empty();
                            $merge_row.children().eq(4).click(function(){$("#merge_countrys").val(JSON.stringify(JSON.parse("["+$("#merge_countrys").val()+"]").concat(JSON.parse("["+$(this).text()+"]")).filter(function(elem, pos, self) {return self.indexOf(elem) == pos;})).replace(/^\[*|\]*$/g,""))});
                            $.ajax({
                                url:"'.SLIM_URL.'api/get_countrys",
                                data: { channel_id: channel_id },
                                success: function(data){
                                    $merge_row.children().eq(4).append(JSON.stringify(data["data"]["countrys"]).replace(/^\[*|\]*$/g,""));
                                }
                            });
                            //alias
                            $merge_row.children().eq(5).empty();
                            $merge_row.children().eq(5).click(function(){$("#merge_alias").val(JSON.stringify(JSON.parse("["+$("#merge_alias").val()+"]").concat(JSON.parse("["+$(this).text()+"]")).filter(function(elem, pos, self) {return self.indexOf(elem) == pos;})).replace(/^\[*|\]*$/g,""))});
                            $.ajax({
                                url:"'.SLIM_URL.'api/get_alias",
                                data: { channel_id: channel_id },
                                success: function(data){
                                    $merge_row.children().eq(5).append(JSON.stringify(data["data"]["alias"]).replace(/^\[*|\]*$/g,""));
                                }
                            });
                            //actions
                            $merge_row.children().eq(6).empty();
                            $bt_merge_cancel = $merge_row.children().eq(6).append("<a href=\"#\">[remove to merge]</a>");
                            $bt_merge_cancel.click(function(){$parent = $(this).parent(); $("#"+$parent.attr("old_id")).show(); $parent.remove();});
                            $merge_table.find("tr").last().before($merge_row);
                            $merge_table.parent().show("blur");
                            return false;
                        });
                        $(".bt_remove").click(function() {
                            if (confirm("Would you realy like delete this channels ?")) {
                                $this = $(this);
                                $.ajax({
                                    url:"'.SLIM_URL.'admin/ajax/channels/"+$this.attr("channel_id")+"/delete",
                                    success: function(data){
                                        $row = $this.parent().parent();
                                        $row.remove();
                                        console.log(data);
                                    }
                                });
                            }
                            return false;
                        });'
    : ''
).'
                    }
                })
            }
        }
        else
        {
            $load.hide("blind");
        }
    }
    $(document).ready(function() {
        var $window = $(window);
        var $channels = $("#channels");
        var $q_name = $("#q_name");
        var $grabber_id = $("#grabber_id");
        var $load = $(".load");
        var searchCallback;
        append_channels();
        $q_name.keyup(function(){
            allLoad = false;
            $load.show("blind");
            $channels.empty();
            clearTimeout(searchCallback);
            searchCallback = setTimeout("append_channels()",1000);
        });
        $grabber_id.change(function(){
            allLoad = false;
            $channels.empty();
            clearTimeout(searchCallback);
            append_channels();
        });
        $window.scroll(function(){
            append_channels ();
        });
        wp_logo_provider = "commons.wikimedia.org";
        $propose_change = $("#propose_change");
        $propose_change.hide();
        $("#bt_submit").click(function(){
            channels = [];
            $(".row_editable_channel").each(function() {
                $this = $(this);
                channels.push({
                    "channel_id": $this.find(".channel_id").val(),
                    "name": $this.find(".channel_name").val(),
                    "country": $this.find(".channel_country").val().split(","),
                    "logo": $this.find(".logo_url").val()
                    //"xmltvid": TODO
                });
            });
            console.log(channels);
            $.ajax({
                url:"'.SLIM_URL.'api/set_channels?",
                type:"post",
                data:{
                    channels: JSON.stringify(channels),
                    email: $("#contrib_name").val()+" <"+$("#contrib_email").val()+">"
                },
                success: function(data){
                    console.log(data);
                    if (data.contribution_id != -1) {
                        window.location.replace("'.SLIM_URL.'contributions/"+data.contribution_id);
                    } else {
                        alert("No change have be recording");
                    }
                }
            });
        });
'.
($user->in_group('admin')
    ? '
        var $merge_table = $("<div style=\"position:fixed; top:0; width:100%; background:white\"><table id=\"merge_table\"><tr><th>Base<sup>(1)</sup></th><th>channel_id</th><th>logo</th><th>name</th><th>countrys</th><th>alias</th><th>actions</th></tr><tr style=\"background:light-green\"><td></td><td><input type=\"text\" id=\"merge_channel_id\" /></td><td><input type=\"text\" id=\"merge_logo\" /></td><td><input type=\"text\" id=\"merge_name\" /></td><td><input type=\"text\" id=\"merge_countrys\" /></td><td><textarea id=\"merge_alias\" /></td><td><a href=\"#\" id=\"confirm_merge\">[Create new channel and remove olders]<a></td></tr></table>Base<sup>(1)</sup>: New channel will use base data of the selected station (xmltvids ...)<br />Tips: Click for insert ! You can edit it !</div>");
        $("body").append($merge_table);
        $merge_table.hide();
        $("#confirm_merge").click(function() {
        $.ajax({
            url :  "'.SLIM_URL.'admin/tools",
            data : {
                do: "replace_channel_id",
                old: $("input[name=merge_base]:checked").val(),
                new: $("#merge_channel_id").val()
            },
            success : function(data) {
                if (window.confirm(data+"\nPress cancel il they are an error")) {
                    $("input[name=merge_base]").each(function(){
                        $this = $(this);
                        if ($this.val() != $("#merge_channel_id").val()) {
                            $.ajax({
                                url:"'.SLIM_URL.'admin/ajax/channels/"+$this.val()+"/delete",
                                success: function(data){
                                    console.log(data);
                                }
                            });
                        }
                        $this.parent().parent().remove()
                    });
                    channels = [{
                        "channel_id": $("#merge_channel_id").val(),
                        "name": $("#merge_name").val(),
                        "country": JSON.parse("["+$("#merge_countrys").val()+"]"),
                        "logo": $("#merge_logo").val(),
                        "alias": JSON.parse("["+$("#merge_alias").val()+"]")
                    }];
                    $.ajax({
                        url:"'.SLIM_URL.'api/set_channels?",
                        type:"post",
                        data:"channels="+encodeURIComponent(JSON.stringify(channels))+"&email="+encodeURIComponent($("#contrib_email").val())+"&grabber_id="+encodeURIComponent("TODO"),
                        success: function(data){
                            console.log(data);
                            if (data.contribution_id != -1) {
                                if (window.confirm("Redirect to contrib page ?")) {
                                    window.location.replace("'.SLIM_URL.'contributions/"+data.contribution_id);
                                }
                            } else {
                                alert("No change have be recording");
                            }
                        }
                    });
                    $("#merge_channel_id").val("");
                    $("#merge_name").val("");
                    $("#merge_countrys").val("");
                    $("#merge_logo").val("");
                    $("#merge_alias").val("");
                    $merge_table.hide("blur");
                }
            }
        });
        return false;
    });
    '
    : ''
).'
    });
</script>';
include 'header.php';

?>

<fieldset>
    <legend>Filter</legend>

    <label for="q_name">Name</label>
    <input type="text" id="q_name" />

    <label for="grabber_id">Grabber</label>
    <select id="grabber_id">
        <?php foreach ($grabbers as $g) echo '<option value="'.$g['grabber_id'].'">'.strtoupper($g['country']).': '.$g['grabber_id'].'</option>'; ?>
    </select>
</fieldset>

<table>
    <thead>
        <tr>
            <th>id</th>
            <th>logo</th>
            <th>name</th>
            <th>country</th>
            <th>xmltvid</th>
            <th>action</th>
        </tr>
    </thead>
    <tbody id="channels">

    </tbody>
</table>


<div class="load load_medium"></div>

<?php
include 'footer_contrib.php';
?>
