<?php
if (!isset($this)) exit(1);
$logo = new image($channel['logo'],100,60)
?>
        <td>
            <?php echo $channel['channel_id'] ?>
            <input class="channel_id" type="hidden" value="<?php echo htmlentities($channel['channel_id']) ?>" />
        </td>
        <td>
            <?php echo $logo->get_htmltag(['id'=>'logo_img_'.$channel['id'],'class'=>'logo_img logo']) ?><br />
            <input class="logo_url" type="text" value="<?php echo htmlentities($channel['logo']) ?>" />
            <br />
            <a class="wp_logo_bt_load" href="#">[load]</a>
            <a class="wp_logo_bt_dialog_toggle" href="#">[search]</a>
            <div class="wp_logo_dialog" id="wp_logo_dialog_<?php echo $channel['id'] ?>">
                <fieldset>
                    <legend>Search</legend>
                    <input type="text" class="wp_logo_query" />
                    <select class="wp_logo_provider" name="language">
                        <option value="commons.wikimedia.org" lang="ar">Wikimédia commun</option>
                        <optgroup label="Wikipédia">
                            <option value="ar.wikipedia.org" lang="ar">العربية</option><!-- Al-ʿArabīyah -->
                            <option value="az.wikipedia.org" lang="az">Azərbaycanca</option>
                            <option value="bg.wikipedia.org" lang="bg">Български</option><!-- Bulgarski -->
                            <option value="ca.wikipedia.org" lang="ca">Català</option>
                            <option value="cs.wikipedia.org" lang="cs">Česky</option>
                            <option value="da.wikipedia.org" lang="da">Dansk</option>
                            <option value="de.wikipedia.org" lang="de">Deutsch</option>
                            <option value="el.wikipedia.org" lang="el">Ελληνικά</option><!-- Ellīniká -->
                            <option value="en.wikipedia.org" lang="en">English</option>
                            <option value="es.wikipedia.org" lang="es">Español</option>
                            <option value="eo.wikipedia.org" lang="eo">Esperanto</option>
                            <option value="et.wikipedia.org" lang="et">Eesti</option>
                            <option value="eu.wikipedia.org" lang="eu">Euskara</option>
                            <option value="fa.wikipedia.org" lang="fa">فارسی</option><!-- Fārsi -->
                            <option value="fr.wikipedia.org" lang="fr">Français</option>
                            <option value="gl.wikipedia.org" lang="gl">Galego</option>
                            <option value="ko.wikipedia.org" lang="ko">한국어</option><!-- Hangugeo -->
                            <option value="hy.wikipedia.org" lang="hy">Հայերեն</option><!-- Hayeren -->
                            <option value="hi.wikipedia.org" lang="hi">हिन्दी</option><!-- Hindī -->
                            <option value="hr.wikipedia.org" lang="hr">Hrvatski</option>
                            <option value="id.wikipedia.org" lang="id">Bahasa Indonesia</option>
                            <option value="it.wikipedia.org" lang="it">Italiano</option>
                            <option value="he.wikipedia.org" lang="he">עברית</option><!-- ‘Ivrit -->
                            <option value="la.wikipedia.org" lang="la">Latina</option>
                            <option value="lt.wikipedia.org" lang="lt">Lietuvių</option>
                            <option value="hu.wikipedia.org" lang="hu">Magyar</option>
                            <option value="ms.wikipedia.org" lang="ms">Bahasa Melayu</option>
                            <option value="min.wikipedia.org" lang="min">Baso Minangkabau</option>
                            <option value="nl.wikipedia.org" lang="nl">Nederlands</option>
                            <option value="ja.wikipedia.org" lang="ja">日本語</option><!-- Nihongo -->
                            <option value="no.wikipedia.org" lang="nb">Norsk (bokmål)</option>
                            <option value="nn.wikipedia.org" lang="nn">Norsk (nynorsk)</option>
                            <option value="uz.wikipedia.org" lang="uz">Oʻzbekcha</option>
                            <option value="pl.wikipedia.org" lang="pl">Polski</option>
                            <option value="pt.wikipedia.org" lang="pt">Português</option>
                            <option value="kk.wikipedia.org" lang="kk">Қазақша / Qazaqşa / قازاقشا</option>
                            <option value="ro.wikipedia.org" lang="ro">Română</option>
                            <option value="ru.wikipedia.org" lang="ru">Русский</option><!-- Russkiy -->
                            <option value="sah.wikipedia.org" lang="sah">Саха Тыла</option><!-- Saxa Tyla -->
                            <option value="simple.wikipedia.org" lang="en">Simple English</option>
                            <option value="ceb.wikipedia.org" lang="ceb">Sinugboanong</option><!-- Cebuano -->
                            <option value="sk.wikipedia.org" lang="sk">Slovenčina</option>
                            <option value="sl.wikipedia.org" lang="sl">Slovenščina</option>
                            <option value="sr.wikipedia.org" lang="sr">Српски / Srpski</option>
                            <option value="sh.wikipedia.org" lang="sh">Srpskohrvatski / Српскохрватски</option>
                            <option value="fi.wikipedia.org" lang="fi">Suomi</option>
                            <option value="sv.wikipedia.org" lang="sv">Svenska</option>
                            <option value="tr.wikipedia.org" lang="tr">Türkçe</option>
                            <option value="uk.wikipedia.org" lang="uk">Українська</option><!-- Ukrayins’ka -->
                            <option value="vi.wikipedia.org" lang="vi">Tiếng Việt</option>
                            <option value="vo.wikipedia.org" lang="vo">Volapük</option>
                            <option value="war.wikipedia.org" lang="war">Winaray</option>
                            <option value="zh.wikipedia.org" lang="zh">中文</option><!-- Zhōngwén -->
                        </optgroup>
                    </select>
                    <input type="button" class="wp_logo_bt" value="Search" />
                    <div class="wp_logo_result"></div>
                </fieldset>
            </div>
        </td>
        <td>
            <input class="channel_name" type="text" value="<?php echo htmlentities($channel['name']) ?>" />
        </td>
        <td>
            Available in <code><?php echo $channel['country'] ?></code>
            <label for="channel_country_<?php echo $channel['id'] ?>">Add other ?</label>
            <input class="channel_country" type="text" /><br />
            (<a class="wp" href="http://en.wikipedia.org/wiki/ISO_3166-2">iso 3166-2 format</a>. For many countrys use separator ",")
        </td>
        <td>
            <!--<?php echo
                (isset($channel['xmltvid']) && $channel['xmltvid'])
                ? '<input class="channel_xmltvid" type="text" value="'.htmlentities($channel['xmltvid']).'"></td>'
                : '-'
            ?>-->
        <td>
            -
        </td>
