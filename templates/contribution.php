<?php
if (!isset($this)) exit(1);

$title = 'Contribution #'.$contribution['id'];

include 'header.php';
?>
<table>
<tr>
    <th>By</th>
    <td><?php echo $contribution['name'] ?></td>
</tr>
<tr>
    <th>On</th>
    <td><?php echo strftime('%c',$contribution['date'] ) ?></td>
</tr>
<tr>
    <th>Comment</th>
    <td><?php echo $contribution['comment'] ?></td>
</tr>
<tr>
    <th>Status</th>
    <td><?php echo (!$contribution['closed'] ? 'pending to accept by moderators' : 'closed on '.strftime('%c',$contribution['closed'] )) ?></td>
</tr>
</table>

<h3>Contains</h3>
<?php
if (!empty($channels))
{
    echo '
        <h4>Channel(s)</h4>
        <table>
            <tr>
                <th>Channel_id</th>
                <th>Name</th>
                <th>Accepted</th>
            </tr>';
    foreach ($channels as $chan) echo '
            <tr>
                <td>'.$chan['channel_id'].'</td>
                <td>'.$chan['name'].'</td>
                <td>'.( $chan['accepted'] ? strftime('%c',$chan['accepted']).' by '.$users[$chan['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($countrys))
{
    echo '
        <h4>Country(s)</h4>
        <table>
            <tr>
                <th>Channel_id</th>
                <th>Country</th>
                <th>Accepted</th>
            </tr>';
    foreach ($countrys as $c) echo '
            <tr>
                <td>'.$c['channel_id'].'</td>
                <td>'.$c['country'].'</td>
                <td>'.( $c['accepted'] ? strftime('%c',$c['accepted']).' by '.$users[$c['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($alias))
{
    echo '
        <h4>Alias</h4>
        <table>
            <tr>
                <th>Channel_id</th>
                <th>Alias (country)</th>
                <th>Accepted</th>
            </tr>';
    foreach ($alias as $al) echo '
            <tr>
                <td>'.$al['channel_id'].'</td>
                <td>'.$al['alias'].'</td>
                <td>'.( $al['accepted'] ? strftime('%c',$al['accepted']).' by '.$users[$al['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($logos))
{
    echo '
        <h4>Logo(s)</h4>
        <table>
            <tr>
                <th>Channel_id</th>
                <th>logos</th>
                <th>Accepted</th>
            </tr>';
    foreach ($logos as $logo)
    {
        $logoimg = new image($logo['url'],100,60);
        echo '
            <tr>
                <td>'.$logo['channel_id'].'</td>
                <td>'.$logoimg->get_htmltag().'</td>
                <td>'.( $logo['accepted'] ? strftime('%c',$logo['accepted']).' by '.$users[$logo['accepted_by']] : 'pending').'</td>
            </tr>';
    }
    echo '
        </table>';
}

if (!empty($grabbers))
{
    echo '
        <h4>Grabbers(s)</h4>
        <table>
            <tr>
                <th>Grabber_id</th>
                <th>Name</th>
                <th>Web</th>
                <th>Accepted</th>
            </tr>';
    foreach ($grabbers as $grab) echo '
            <tr>
                <td>'.$grab['grabber_id'].'</td>
                <td>'.$grab['name'].' ('.$grab['country'].')</td>
                <td><a href="'.$grab['web'].'">'.$grab['web'].'</a></td>
                <td>'.( $grab['accepted'] ? strftime('%c',$grab['accepted']).' by '.$users[$grab['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($xmltvids))
{
    echo '
        <h4>Grabbers(s)</h4>
        <table>
            <tr>
                <th>Channel_id</th>
                <th>Grabber_id</th>
                <th>xmltvid</th>
                <th>Accepted</th>
            </tr>';
    foreach ($xmltvids as $xmltvid) echo '
            <tr>
                <td>'.$xmltvid['channel_id'].'</td>
                <td>'.$xmltvid['grabber_id'].'</td>
                <td>'.$xmltvid['xmltvid'].' ('.$al['grab'].')</td>
                <td>'.( $grab['accepted'] ? strftime('%c',$grab['accepted']).' by '.$users[$grab['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($tvpackages))
{
    echo '
        <h4>TV Package(s)</h4>
        <table>
            <tr>
                <th>tvpackage_id</th>
                <th>Path</th>
                <th>Name</th>
                <th>Accepted</th>
            </tr>';
    foreach ($tvpackages as $tvpkg) echo '
            <tr>
                <td>'.$tvpkg['tvpackage_id'].'</td>
                <td>'.$tvpkg['path'].'</td>
                <td>'.$tvpkg['name'].'</td>
                <td>'.( $tvpkg['accepted'] ? strftime('%c',$tvpkg['accepted']).' by '.$users[$tvpkg['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

if (!empty($tvpackageschannels))
{
    echo '
        <h4>Channel(s) in TV Package(s)</h4>
        <table>
            <tr>
                <th>tvpackage_id</th>
                <th>channel_id</th>
                <th>Accepted</th>
            </tr>';
    foreach ($tvpackageschannels as $tvpkgchan) echo '
            <tr>
                <td>'.$tvpkgchan['tvpackage_id'].'</td>
                <td>'.$tvpkgchan['channel_id'].'</td>
                <td>'.( $tvpkgchan['accepted'] ? strftime('%c',$tvpkgchan['accepted']).' by '.$users[$tvpkgchan['accepted_by']] : 'pending').'</td>
            </tr>';
    echo '
        </table>';
}

include 'footer.php';
?>
