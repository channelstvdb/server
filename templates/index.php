<?php
if (!isset($this)) exit(1);

$title = 'Channels';

include 'header.php';
?>

<div>
    <p>Welcome !</p>
</div>

<div>
    <h3>Help</h3>
    <ul>
        <li><a href="http://mythtv-fr.org/wiki/channelstvdb">Documentation and terms of use</a></li>
        <li>Support: <a href="http://mythtv-fr.org/forums/viewforum.php?id=10">Forum</a>, <a href="http://mythtv-fr.org/forums/chat.php">Chat</a></li>
        <li><a href="http://mythtv-fr.org/bugtracker/index.php?project=8">Bugtracker</a></li>
    </ul>
</div>

<?php
include 'footer.php';
?>
