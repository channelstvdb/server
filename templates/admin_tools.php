<?php
if (!isset($this)) exit(1);

$title = 'admin - tools';
$add_header = '
<script type="text/javascript" src="'.BASE_URL.'js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="'.BASE_URL.'js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $alert = $("#alert");
            $alert.hide();
            $("#replace_channel_id_submit").click(function() {
                $.ajax({
                    url :  "'.SLIM_URL.'admin/tools",
                    data : {
                        do: "replace_channel_id",
                        old: $("#replace_channel_id_old").val(),
                        new: $("#replace_channel_id_new").val()
                    },
                    success : function(data) {
                        console.log(data);
                        $alert.text(data);
                        $alert.hide();
                        $alert.show("highlight",500);
                    }
                });

            });
        });
</script>';


include 'header.php';
?>

<div id="alert"></div>

<div style="float:right">
    <ul>
        <li><a href="#alias">Alias</a></li>
        <li><a href="#channels">Channels</a></li>
    </ul>
</div>

<h3 id="alias">Alias</h3>

<h4>Replacements</h4>

<p>Replace chars in alias for alias cleaners.</p>

<a href="<?php echo SLIM_URL ?>admin/tools/alias_cleaner">Manager</a>


<h3 id="channels">Channels</h3>

<h4>Replace id</h4>

<p>Replace an channel_id</p>

<label for="replace_channel_id_old">Old channel id</label><input type="text" id="replace_channel_id_old" />
<label for="replace_channel_id_new">New channel id</label><input type="text" id="replace_channel_id_new" />
<input type="button" id="replace_channel_id_submit" value="Replace" />


<?php
include 'footer.php';
?>
