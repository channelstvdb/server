<?php
if (!isset($this)) exit(1);

$email = parse_email($user->emailRFC);

$add_footer = '
<div id="propose_change" style="position:fixed; bottom:0px; width:100%; background-color:LightGoldenRodYellow ; text-align:center; border-top: 1px solid orange;">
    <input id="bt_submit" type="button" value="Propose change" />
    (<label for="contrib_name">Name<sup>1</sup></label><input id="contrib_name" type="texte" value="'.htmlentities($email['name']).'" />
    <label for="contrib_email">E-mail<sup>2</sup></label><input id="contrib_email" type="texte" value="'.htmlentities($email['email']).'" />)
    <div style="font-size:50%">
        Name<sup>1</sup> optionnal if you want to see your name in the list of contributions —
        E-mail<sup>2</sup> optionnal if you want to be notified of processing your contribution.</div>
</div>
</form>';

include 'footer.php';
?>
