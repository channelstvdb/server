<?php
if (!isset($this)) exit(1);

$title = 'Admin - Alias cleaner manager';
include 'header.php';
?>

<table>

    <tr>
        <th>#</th>
        <th>Search</th>
        <th>Replace</th>
        <th>Action</th>
    </tr>
<?php foreach ($remplacements as $r) { ?>
    <tr>
        <form method="post" action="<?php echo SLIM_URL ?>admin/tools/alias_cleaner" >
        <td><input type="hidden" name="id" value="<?php echo $r['id'] ?>"/><?php echo $r['id'] ?></td>
        <td><input type="text" name="search" value="<?php echo $r['search'] ?>"/></td>
        <td><input type="text" name="replace" value="<?php echo $r['replace'] ?>"/></td>
        <td><input type="submit" name="update" value="Update"/><input type="submit" name="delete" value="Delete"/></td>
        </form>
    </tr>
<?php } ?>
    <form method="post" action="<?php echo SLIM_URL ?>admin/tools/alias_cleaner" >
    <tr>
        <td>-</td>
        <td><input type="text" name="search" /></td>
        <td><input type="text" name="replace" /></td>
        <td><input type="submit" name="add" value="Add"/></td>
    </tr>
    </form>
</table>
<!--
<form method="post" action="<?php echo SLIM_URL ?>admin/tools/alias_cleaner" >
    <input type="text" name="search" />
    <input type="text" name="replace" />
    <input type="submit" name="add" value="Add"/>
</form>
-->

<?php
include 'footer.php';
?>
