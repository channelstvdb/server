<?php
if (!isset($this)) exit(1);

$title = 'admin - contributions';
$add_header = '
<script type="text/javascript" src="'.BASE_URL.'js/jquery-1.10.2.js"></script>
<script type="text/javascript">
        function close_contribution(data) {
            if (data["closed"]) {
                $("#box_contribution_"+data["contribution_id"]).hide("bind");
            }
        }

        $(document).ready(function() {
            $(".contribution").css("cursor","pointer");
            $(".contribution").click(function() {
                var $contribution_id = $(this).attr("contribution_id");
                var $inbox = $("#inbox_contribution_"+$contribution_id);
                $(".inbox_contribution:not(#inbox_contribution_"+$contribution_id+")").hide("bind");
                if ($inbox.is(":empty")) {
                    $inbox.load("'.SLIM_URL.'admin/ajax/contributions/"+$contribution_id, function(){
                        $bt_accept = $(this).find(".accept");
                        $bt_accept.css("cursor","pointer");
                        $bt_accept.click(function() {
                            $.getJSON("'.SLIM_URL.'admin/ajax/"+$(this).attr("el_type")+"/"+$(this).attr("el_id")+"/accept", function( data ) { close_contribution(data) });
                            $("#tbody_"+$(this).attr("el_type")+"_"+$(this).attr("el_id")).fadeOut(200);
                        });
                        $bt_refused = $inbox.find(".refused");
                        $bt_refused.css("cursor","pointer");
                        $bt_refused.click(function() {
                            $.getJSON("'.SLIM_URL.'admin/ajax/"+$(this).attr("el_type")+"/"+$(this).attr("el_id")+"/refused", function( data ) { close_contribution(data) });
                            $("#tbody_"+$(this).attr("el_type")+"_"+$(this).attr("el_id")).fadeOut(200);
                        });

                    });
                    $(".accept").css("cursor","pointer");
                    $inbox.find(".accept").click(function() {
                        $span = $(this);
                        $span.css("cursor","pointer");
                        alert($span.attr("el_id"));
                    });
                    $inbox.show("bind");
                } else {
                    $inbox.hide("bind");
                    $inbox.empty();
                }
            });
        });
</script>';


include 'header.php';

foreach ($this->data->contributions as $contribution) {
?>
    <div id="box_contribution_<?php echo $contribution['id'] ?>">
        <h3 class="contribution" contribution_id="<?php echo $contribution['id'] ?>">
            <?php echo '#'.$contribution['id'].' - '.$contribution['comment']. ' (by '.($contribution['email'] ? htmlspecialchars($contribution['email']) : 'unknow').' on '. strftime('%c', $contribution['date']).')' ?>
        </h3>
        <div id="inbox_contribution_<?php echo $contribution['id'] ?>" class="inbox_contribution"></div>
    </div>
<?php
}

include 'footer.php';
?>
