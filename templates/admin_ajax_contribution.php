<?php
if (!isset($this)) exit(1);
echo '
        <div style="float:left">
            <table>
                <caption>Channels</caption>
                <thead>
                    <tr class="new">
                        <th>Channel ID</th>
                        <th>Old name</th>
                        <th>New name</th>
                        <th>Action</th>
                    </tr>
                </thead>';
foreach ($channels as $channel) {
    echo '
                <tbody id="tbody_channels_'.$channel['id'].'">
                    <tr>
                        <th>'.$channel['channel_id'].'</th>
                        <td>'.$channel['old_name'].'</td>
                        <td>'.$channel['new_name'].'</td>
                        <td>
                            <span class="accept" el_type="channels" el_id="'.$channel['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="channels" el_id="'.$channel['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
        }
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>Country</caption>
                <thead>
                    <tr class="new">
                        <th>channel ID</th>
                        <th>Country</th>
                        <th>Action</th>
                    </tr>
                </thead>
    ';
foreach ($channelscountrys as $cc) {
    echo '
                <tbody id="tbody_channelscountrys_'.$cc['id'].'">
                    <tr class="old">
                        <th>'.$cc['channel_id'].'</th>
                        <td>'.$cc['country'].'</td>
                        <td>
                            <span class="accept" el_type="channelscountrys" el_id="'.$cc['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="channelscountrys" el_id="'.$cc['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>Alias</caption>
                <thead>
                    <tr class="new">
                        <th>channel ID</th>
                        <th>Old</th>
                        <th>New</th>
                        <th>Actions</th>
                    </tr>
                </thead>
    ';
foreach ($alias as $al) {
    echo '
                <tbody id="tbody_alias_'.$al['id'].'">
                    <tr class="old">
                        <th>'.$al['channel_id'].'</th>
                        <td>'.$al['alias'].'</td>
                        <td>
                            <span class="accept" el_type="alias" el_id="'.$al['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="alias" el_id="'.$al['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>Logos</caption>
                <thead>
                    <tr class="new">
                        <th>channel ID</th>
                        <th>Old</th>
                        <th>New</th>
                        <th>Actions</th>
                    </tr>
                </thead>
    ';
foreach ($logos as $logo) {
    $old = new image($logo['old_url']);
    $new = new image($logo['new_url']);
    echo '
                <tbody id="tbody_logos_'.$logo['id'].'">
                    <tr class="old">
                        <th>'.$logo['channel_id'].'</th>
                        <td>'.$old->get_htmltag(['class'=>'logo'],'empty').'</td>
                        <td>'.$new->get_htmltag(['class'=>'logo'],'empty').'</td>
                        <td>
                            <span class="accept" el_type="logos" el_id="'.$logo['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="logos" el_id="'.$logo['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>Grabbers</caption>
                    <thead>
                        <tr class="new">
                            <th>grabber ID</th>
                            <th>Old</th>
                            <th>New</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
    ';
foreach ($grabbers as $grabber) {
    echo '
                <tbody id="tbody_grabbers_'.$grabber['id'].'">
                    <tr class="old">
                        <th>'.$grabber['grabber_id'].'</th>
                        <td>'.$grabber['old_name'].' ('.$grabber['old_country'].')</td>
                        <td>'.$grabber['new_name'].' ('.$grabber['new_country'].')</td>
                        <td>
                            <span class="accept" el_type="grabbers" el_id="'.$grabber['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="grabbers" el_id="'.$grabber['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>xmltvids</caption>
                <thead>
                    <tr class="new">
                        <th>channel ID</th>
                        <th>grabber ID</th>
                        <th>Old</th>
                        <th>New</th>
                        <th>Actions</th>
                    </tr>
                </thead>
    ';
foreach ($xmltvids as $xmltvid) {
    echo '
                <tbody id="tbody_xmltvids_'.$xmltvid['id'].'">
                    <tr class="old">
                        <th>'.$xmltvid['channel_id'].'</th>
                        <th>'.$xmltvid['grabber_id'].'</th>
                        <td>'.$xmltvid['old_xmltvid'].'</td>
                        <td>'.$xmltvid['new_xmltvid'].'</td>
                        <td>
                            <span class="accept" el_type="xmltvids" el_id="'.$xmltvid['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="xmltvids" el_id="'.$xmltvid['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
            </table>
        </div>
        <div style="float:left">
            <table>
            <caption>tvpackages</caption>
            <thead>
                <tr class="new">
                    <th>tvpackage ID</th>
                    <th colspan="2">Old</th>
                    <th colspan="2">New</th>
                    <th>Actions</th>
                </tr>
            </thead>
        ';
foreach ($tvpackages as $tvpackage) {
    echo '
                <tbody id="tbody_tvpackages_'.$tvpackage['id'].'">
                    <tr class="old">
                        <th>'.$tvpackage['tvpackage_id'].'</th>
                        <td>'.$tvpackage['old_path'].'</td>
                        <td>'.$tvpackage['old_name'].'</td>
                        <td>'.$tvpackage['new_path'].'</td>
                        <td>'.$tvpackage['new_name'].'</td>
                        <td>
                            <span class="accept" el_type="tvpackages" el_id="'.$tvpackage['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="tvpackages" el_id="'.$tvpackage['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
                </table>
            </div>
            <div style="float:left">
                <table>
                <caption>xmltvids</caption>
                <thead>
                    <tr class="new">
                        <th>tvpackage ID</th>
                        <th>channel ID</th>
                        <th>old chan num</th>
                        <th>new chan num</th>
                        <th>Actions</th>
                    </tr>
                </thead>
    ';
foreach ($tvpackageschannels as $tvpackagechannel) {
    echo '
                <tbody id="tbody_tvpackageschannels_'.$tvpackagechannel['id'].'">
                    <tr class="old">
                        <td>'.$tvpackagechannel['tvpackage_id'].'</td>
                        <td>'.$tvpackagechannel['channel_id'].'</td>
                        <td>'.$tvpackagechannel['old_channel_num'].'</td>
                        <td>'.$tvpackagechannel['new_channel_num'].'</td>
                        <td>
                            <span class="accept" el_type="tvpackageschannels" el_id="'.$tvpackagechannel['id'].'" style="color:green">[accept]</span>
                            <span class="refused" el_type="tvpackageschannels" el_id="'.$tvpackagechannel['id'].'" style="color:red">[refused]</span>
                        </td>
                    </tr>
                </tbody>';
}
echo '
            </table>
        </div>
        <div style="clear:both">
            <span class="accept" el_type="contributions" el_id="'.$contribution['id'].'" style="color:green">[accept all]</span>
            <span class="refused" el_type="contributions" el_id="'.$contribution['id'].'" style="color:red">[refused all]</span>
        </div>
        ';



?>



