<?php
if (!isset($this)) exit(1);
?><<?php ?>?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>ChannelsTVDB.mythtv-fr.org - <?php echo isset($title) ? $title : '' ?></title>
        <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL?>/css/base.css" />

<?php echo isset($add_header) ? $add_header : '' ?>
</head>
<body>
    <div class="title">
        <h1><a href="<?php echo SLIM_URL ?>">ChannelsTVDB.mythtv-fr.org</a></h1>
        <p>Online database for channels logos TV and xmltvids</p>
    </div>
    <div class="menu">
        <ul class="main_menu">
            <li><a href="<?php echo SLIM_URL ?>channels">Channels</li>
            <li><a href="<?php echo SLIM_URL ?>alias/unknow">Unknow alias</li>
<?php if ((isset($this->data->user)) && ($this->data->user->in_group('moderator'))) { ?>
            <li><a href="<?php echo SLIM_URL ?>admin/contributions/pendings">Pendings contributions</li>
<?php }
      if ((isset($this->data->user)) && ($this->data->user->in_group('admin'))) { ?>
            <li><a href="<?php echo SLIM_URL ?>admin/users">Admin users</li>
            <li><a href="<?php echo SLIM_URL ?>admin/tools">Admin tools</li>
<?php } ?>
        </ul>
        <ul class="user_menu">
<?php if ((!isset($this->data->user)) || ($this->data->user->is_guest())) { ?>
            <li><a href="<?php echo SLIM_URL ?>login">Login / subscribe</a></li>
<?php } else { ?>
            <li><a href="<?php echo SLIM_URL ?>profil"><?php echo $this->data->user->name ?></a></li>
            <li><a href="<?php echo SLIM_URL ?>logout">Logout</a></li>
<?php } ?>
        </ul>
    </div>
    <div class="content">
    <?php echo isset($title) ? "<h2>$title</h2>" : '' ?>
