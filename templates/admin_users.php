<?php
if (!isset($this)) exit(1);

$title = 'admin - users';
$add_header = '
<script type="text/javascript" src="'.BASE_URL.'js/jquery-1.10.2.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $( ".checkbox_user_in_group" ).change(function(){
                var $input = $( this );
                if (this.checked) {
                    $.getJSON( "'.SLIM_URL.'admin/groups/"+$input.attr("group_id")+"/add_user/"+$input.attr("user_id") );
                } else {
                    $.getJSON( "'.SLIM_URL.'admin/groups/"+$input.attr("group_id")+"/remove_user/"+$input.attr("user_id") );
                }
            });
            $( ".accept_user" ).click(function(){
                var $input = $( this );
                $.getJSON( "'.SLIM_URL.'admin/ajax/users/"+$input.attr("user_id")+"/accept" );
                location.reload();
                return false;
            });
        });
</script>';


include 'header.php';

?>


<h3>Users / groups</h3>

<table>
    <tr>
        <th rowspan="2">#</th>
        <th rowspan="2">name</th>
        <th rowspan="2">email</th>
        <th rowspan="2">openid_identifier</th>
        <th colspan="<?php echo count($this->data['groups']) ?>">groups</th>
    </tr>
    <tr>
<?php
foreach ($this->data->groups as $group) {
    echo '
        <th>'.$group['name'].'</th>';
}
?>
    </tr>
<?php
foreach ($this->data->users as $user) {
    echo '
    <tr>
    <form action="" method="post">
        <td>'.$user['id'].'<input type="hidden" name="userid" value="'.$user['id'].'"</td>
        <td>'.$user['name'].'</td>
        <td><a href="mailto:'.$user['email'].'">'.$user['email'].'</a></td>
        <td>'.$user['openid_identifier'].'</td>';

    foreach ($this->data['groups'] as $group) {
        echo '
        <td><input class="checkbox_user_in_group" type="checkbox" user_id="'.$user['id'].'" group_id="'.$group['id'].'" '.(in_array($user['id'], $group['users']) ? 'checked="checked" ' : '').'name="groups[]" value="'.$group['id'].'" />';
    }
    echo '
    </form>
    </tr>';
}
?>
</table>


<h3>Users registrations pendings</h3>

<?php if (empty($this->data['users_registration_pending'])) {?>
<p>They are not users registrations pendings.</p>
<?php } else { ?>
<table>
    <tr>
        <th>#</th>
        <th>name</th>
        <th>email</th>
        <th>openid</th>
        <th>tools</th>
    </tr>
 <?php
foreach ($this->data['users_registration_pending'] as $user) {
    echo '
    <tr>
    <form action="" method="post">
        <td>'.$user['id'].'<input type="hidden" name="userid" value="'.$user['id'].'"</td>
        <td>'.$user['name'].'</td>
        <td><a href="mailto:'.$user['email'].'">'.$user['email'].'</a></td>
        <td>'.$user['openid_identifier'].'</td>
        <td><input type="submit" class="accept_user" user_id="'.$user['id'].'"  value="accept"> <input type="submit" name="reject_user" value="reject" /></td>
    </form>
    </tr>';
}
?>
</table>
<?php } ?>



