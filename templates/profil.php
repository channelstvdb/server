<?php
if (!isset($this)) exit(1);

$title = 'Profil';
include 'header.php';
?>

<form action="profil" method="post">
    <p>
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="<?php echo $this->data->user->name ?>" />
    </p>
    <p>
        <label for="email">Email:</label>
        <input type="text" id="email" name="email" value="<?php echo $this->data->user->email ?>" />
    </p>
    <p>
        <input type="submit" name="submit_change" value="Submit change" />
    </p>
<?php if ($this->data->user->accepted) { ?>
    <p style="border: 1px green solid; background-color:lightgreen">Your registration has been accepted the <?php echo strftime('%c', $this->data->user->accepted) ?>.</p>
<?php } else { ?>

    <p style="border: 1px red solid; background-color:pink">Your registration is pending validation by an administrator.</p>
<?php } ?>
</form>
