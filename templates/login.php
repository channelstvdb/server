<?php
if (!isset($this)) exit(1);

$title = 'Login/subscribe';
$add_header = '
        <!-- Simple OpenID Selector -->
        <link type="text/css" rel="stylesheet" href="../css/openid.css" />
        <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="../js/openid-jquery.js"></script>
        <script type="text/javascript" src="../js/openid-en.js"></script>
        <script type="text/javascript">
                $(document).ready(function() {
                        openid.init(\'openid_identifier\');
                });
        </script>
        <!-- /Simple OpenID Selector -->
        <style type="text/css">
                /* Basic page formatting */
                body {
                        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                }
        </style>
</head>';
include 'header.php';
?>

        <p>Login/subscribe with an openid (Your subscription must be validated by an administrator).</p>
        <br/>
        <!-- Simple OpenID Selector -->
        <form action="login" method="get" id="openid_form">
                <input type="hidden" name="action" value="verify" />
                        <div id="openid_choice">
                                <p>Please click your account provider:</p>
                                <div id="openid_btns"></div>
                        </div>
                        <div id="openid_input_area">
                                <input id="openid_identifier" name="openid_identifier" type="text" value="http://" />
                                <input id="openid_submit" type="submit" value="Sign-In"/>
                        </div>
                        <noscript>
                                <p>OpenID is service that allows you to log-on to many different websites using a single indentity.
                                Find out <a href="http://openid.net/what/">more about OpenID</a> and <a href="http://openid.net/get/">how to get an OpenID enabled account</a>.</p>
                        </noscript>
        </form>
        <!-- /Simple OpenID Selector -->
<?php
include 'footer.php';
?>


